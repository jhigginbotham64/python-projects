# my python projects

Playing with mazes while working through Jamis Buck's neat book on the subject:
http://www.mazesforprogrammers.com/

Also building a ray tracer:
http://www.raytracerchallenge.com/

Pillow dependencies (minus openjpeg for JPEG 2000):
```
$ sudo apt-get install libjpeg-dev libtiff-dev libfreetype6 libwebp-dev liblcms2-dev libpng-dev zlib1g-dev
```

I was able to get autocompletion to work in VS Code by adding the following to .vscode/settings.json:

```json
"python.autoComplete.extraPaths": [
    "MY_LOCAL_REPO/mazes"
  ]
```

Machine epsilon in NumPy as per [this SO post](https://stackoverflow.com/a/19141711):
```python
print(np.finfo(float).eps)
# 2.22044604925e-16

print(np.finfo(np.float32).eps)
# 1.19209e-07

print(np.finfo(np.float64).eps)
# 2.22044604925e-16
```

Failing to install OpenCV's dependencies before installing the opencv-python wheel leads to major errors:
https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html#linux-installation
import random
import string


def n_to_base(n, b):
    """
      https://stackoverflow.com/a/28666223
      returns a list of digits of n when converted to base b.
      both n and b are expected to be integers, and the result
      is a list of integers.
    """
    if n == 0:
        return [0]
    digits = []
    while n:
        digits.append(int(n % b))
        n //= b
    return digits[::-1]


def to_alphabet(n):
    if n >= 0 and n <= 9:
        return str(n)
    else:
        return chr(n + 87)


def n_to_base_str(n, b):
    """ 
      converts n_to_base's result into a string, which
      is my actual use case.

      bases larger than 36 risk getting weird characters in 
      the result. for instance n_to_base_str(36,37) == '{'.
    """
    return ''.join(map(to_alphabet, n_to_base(n, b)))


def shuffle(l):
    """
        random.shuffle shuffles in place and doesn't return anything,
        so i find myself using this recommended workaround a lot:
        https://docs.python.org/3/library/random.html#random.shuffle
        "To shuffle an immutable sequence and return a new shuffled list, use sample(x, k=len(x)) instead."
    """
    return random.sample(l, k=len(l))


def random_ascii_letter_string(length: int) -> str:
    return ''.join(random.choice(string.ascii_letters)
                   for i in range(length))


def truncate_float(f, n):
    """ 
        https://stackoverflow.com/a/783927
        truncates/pads a float f to n decimal places without rounding
    """
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    # the conversion back to float is my own addition
    return float('.'.join([i, (d+'0'*n)[:n]]))


def strip_0(n):
    return f"{n:f}".rstrip('0')

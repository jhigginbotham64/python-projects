import os
import pytest

FIXTURE_DIR = os.path.join(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'fixtures'
    ),
    'mazes'
)

MASK_DIR = os.path.join(FIXTURE_DIR, 'masks')

IMG_MASK_DIR = os.path.join(MASK_DIR, 'img')

TXT_MASK_DIR = os.path.join(MASK_DIR, 'txt')

POV_DIR = os.path.join(FIXTURE_DIR, 'pov')


TXT_MASKS = pytest.mark.datafiles(
    os.path.join(TXT_MASK_DIR, 'mask1.txt'),
    os.path.join(TXT_MASK_DIR, 'mask2.txt'),
)

IMG_MASKS = pytest.mark.datafiles(
    os.path.join(IMG_MASK_DIR, 'smiley.png'),
)

from __future__ import annotations


from collections import deque
from random import choice, randrange
from typing import Tuple, List, Callable, Dict


# it's my party
# i can import what i want
from util import *
from mazes.cell import *
from mazes.distances import *
from mazes.mask import *
from mazes.grid import *

# TODO adapt BinaryTree and Sidewinder to work with HexGrid


def BinaryTree(grid: Grid) -> Grid:
    for c in grid.cell_list():
        neighbors = []
        if c.north is not None:
            neighbors.append(c.north)
        if c.east is not None:
            neighbors.append(c.east)

        if len(neighbors):
            c.link(choice(neighbors))

    return grid


def Sidewinder(grid: Grid) -> Grid:
    for r in grid.row_list():
        run = []

        for c in r:
            run.append(c)

            at_eastern_boundary = c.east is None
            at_northern_boundary = c.north is None

            should_close_out = at_eastern_boundary or (
                not at_northern_boundary and choice([0, 1]) == 0)

            if should_close_out:
                member = choice(run)
                if member.north:
                    member.link(member.north)
                run = []
            else:
                c.link(c.east)

    return grid


def AldousBroder(grid: Grid) -> Grid:
    cell = grid.random_cell()
    unvisited = grid.size() - 1

    while unvisited > 0:
        neighbor = choice(cell.neighbors())

        if not neighbor.linked_cells():
            cell.link(neighbor)
            unvisited -= 1

        cell = neighbor

    return grid


def Wilsons(grid: Grid) -> Grid:
    unvisited = [c for c in grid.cell_list()]
    first = choice(unvisited)
    unvisited.remove(first)

    while unvisited:
        cell = choice(unvisited)
        path = [cell]

        while cell in unvisited:
            cell = choice(cell.neighbors())
            if cell in path:
                path = path[0:path.index(cell)]
            else:
                path.append(cell)

        for i in range(len(path) - 1):
            path[i].link(path[i+1])
            unvisited.remove(path[i])

    return grid


def HuntAndKill(grid: Grid) -> Grid:
    current = grid.random_cell()

    while current:
        # unvisited neighbors = neighbors with no links
        unvisited_neighbors = [
            c for c in current.neighbors() if not c.linked_cells()]

        if unvisited_neighbors:
            neighbor = choice(unvisited_neighbors)
            current.link(neighbor)
            current = neighbor
        else:
            current = None
            for cell in grid.cell_list():
                visited_neighbors = [
                    c for c in cell.neighbors() if c.linked_cells()]
                if not cell.linked_cells() and visited_neighbors:
                    current = cell
                    neighbor = choice(visited_neighbors)
                    current.link(neighbor)
                    break

    return grid


def RecursiveBacktracker(grid: Grid, start_at: Optional[Cell] = None) -> Grid:
    if start_at is None:
        start_at = grid.random_cell()
    stack = deque()
    stack.append(start_at)
    while len(stack) > 0:
        current = stack[-1]
        neighbors = [c for c in current.neighbors() if not c.linked_cells()]

        if not neighbors:
            stack.pop()
        else:
            neighbor = choice(neighbors)
            current.link(neighbor)
            stack.append(neighbor)

    return grid


class Kruskals:

    class State:
        grid: Grid
        neighbors: List[Tuple[Cell, Cell]]
        set_for_cell: Dict[Cell, int]
        cells_in_set: Dict[int, List[Cell]]

        def __init__(self, grid: Grid) -> None:
            self.grid = grid
            self.neighbors = []
            self.set_for_cell = {}
            self.cells_in_set = {}

            for cell in self.grid.cell_list():
                setnum = len(self.set_for_cell)
                self.set_for_cell[cell] = setnum
                self.cells_in_set[setnum] = [cell]
                if cell.south:
                    self.neighbors.append((cell, cell.south))
                if cell.east:
                    self.neighbors.append((cell, cell.east))

        def can_merge(self, left: Cell, right: Cell) -> bool:
            return self.set_for_cell[left] != self.set_for_cell[right]

        def merge(self, left: Cell, right: Cell) -> None:
            left.link(right)
            winner = self.set_for_cell[left]
            # dict.get is used because add_crossing involves merging with UnderCells
            # not accounted for in set_for_cell or cells_in_set. this is also why
            # losers is either a cells_in_set reference or simply the right cell.
            loser = self.set_for_cell.get(right)
            losers = self.cells_in_set.get(loser) or [right]  # freakin' Python
            for cell in losers:
                self.cells_in_set[winner].append(cell)
                self.set_for_cell[cell] = winner

            if loser:
                del self.cells_in_set[loser]

        # only really supported for WeaveGrids
        def add_crossing(self, cell: Cell) -> bool:
            if cell.links or not self.can_merge(cell.east, cell.west) or not self.can_merge(cell.north, cell.south):
                return False

            self.neighbors = [
                (left, right) for left, right in self.neighbors if left != cell and right != cell]

            if randrange(2) == 0:
                self.merge(cell.west, cell)
                self.merge(cell, cell.east)
                self.grid.tunnel_under(cell)
                self.merge(cell.north, cell.north.south)
                self.merge(cell.south, cell.south.north)
            else:
                self.merge(cell.north, cell)
                self.merge(cell, cell.south)
                self.grid.tunnel_under(cell)
                self.merge(cell.west, cell.west.east)
                self.merge(cell.east, cell.east.west)

            return True

    def __new__(cls, grid: Grid, state: Optional[State] = None) -> Grid:
        if not state:
            state = Kruskals.State(grid)

        neighbors = shuffle(state.neighbors)

        while neighbors:
            left, right = neighbors.pop()
            if state.can_merge(left, right):
                state.merge(left, right)

        return grid


def SimplifiedPrims(grid: Grid, start_at: Optional[Cell] = None) -> Grid:
    active = [start_at or grid.random_cell()]

    while active:
        cell = choice(active)
        available_neighbors = [
            c for c in cell.neighbors() if not c.linked_cells()]
        if available_neighbors:
            neighbor = choice(available_neighbors)
            cell.link(neighbor)
            active.append(neighbor)
        else:
            active.remove(cell)

    return grid


def TruePrims(grid: Grid, start_at: Optional[Cell] = None) -> Grid:
    active = [start_at or grid.random_cell()]
    costs = {c: randrange(100) for c in grid.cell_list()}
    def cost(c): return costs[c]

    while active:
        active.sort(key=cost)
        cell = active[0]
        available_neighbors = [
            c for c in cell.neighbors() if not c.linked_cells()]

        if available_neighbors:
            neighbor = sorted(available_neighbors, key=cost)[0]
            cell.link(neighbor)
            active.append(neighbor)
        else:
            active.remove(cell)

    return grid


def GrowingTree(selector: Callable[[List[Cell]], Cell]) -> Callable[..., Grid]:
    def gt(grid: Grid, start_at: Optional[Cell] = None) -> Grid:
        active = [start_at or grid.random_cell()]

        while active:
            cell = selector(active)
            available_neighbors = [
                c for c in cell.neighbors() if not c.linked_cells()]

            if available_neighbors:
                neighbor = choice(available_neighbors)
                cell.link(neighbor)
                active.append(neighbor)
            else:
                active.remove(cell)

        return grid
    return gt


class Ellers:
    class RowState:
        set_for_cell: Dict[int, int]
        cells_in_set: Dict[int, List[Cell]]
        next_set: int

        def __init__(self, starting_set=0) -> None:
            self.set_for_cell = {}
            self.cells_in_set = {}
            self.next_set = starting_set

        def record(self, setnum: int, cell: Cell) -> None:
            self.set_for_cell[cell.column] = setnum
            if not self.cells_in_set.get(setnum):
                self.cells_in_set[setnum] = []
            self.cells_in_set[setnum].append(cell)

        def set_for(self, cell: Cell) -> int:
            if cell.column not in self.set_for_cell:
                self.record(self.next_set, cell)
                self.next_set += 1
            return self.set_for_cell[cell.column]

        def merge(self, winner: int, loser: int) -> None:
            for cell in self.cells_in_set[loser]:
                self.set_for_cell[cell.column] = winner
                self.cells_in_set[winner].append(cell)
            del self.cells_in_set[loser]

        def next(self) -> RowState:
            return Ellers.RowState(self.next_set)

        def sets(self) -> Iterable[Tuple[int, List[Cell]]]:
            for setnum, cells in self.cells_in_set.items():
                yield (setnum, cells)

    def __new__(cls, grid: Grid) -> Grid:
        row_state = Ellers.RowState()

        for row in grid.row_list():
            for cell in row:
                if not cell.west:
                    continue

                setnum = row_state.set_for(cell)
                prior_set = row_state.set_for(cell.west)

                should_link = setnum != prior_set and (
                    not cell.south or randrange(2) == 0)

                if should_link:
                    cell.link(cell.west)
                    row_state.merge(prior_set, setnum)

            if row[0].south:
                next_row = row_state.next()

                for setnum, cells in row_state.sets():
                    for index, cell in enumerate(shuffle(cells)):
                        if index == 0 or randrange(3) == 0:
                            cell.link(cell.south)
                            next_row.record(
                                row_state.set_for(cell), cell.south)

                row_state = next_row

        return grid


def RecursiveDivision(grid: Grid, max_room_size: float = -math.inf) -> Grid:
    def divide(row: int, column: int, height: int, width: int) -> None:
        if height <= 1 or width <= 1 or (height < max_room_size and width < max_room_size and randrange(4) == 0):
            return

        if height > width:
            divide_horizontally(row, column, height, width)
        elif height < width:
            divide_vertically(row, column, height, width)
        else:
            if randrange(2):
                divide_horizontally(row, column, height, width)
            else:
                divide_vertically(row, column, height, width)

    def divide_horizontally(row: int, column: int, height: int, width: int) -> None:
        divide_south_of = randrange(height - 1)
        passage_at = randrange(width)

        for x in range(width):
            if x == passage_at:
                continue

            cell = grid(row + divide_south_of, column + x)
            cell.unlink(cell.south)

        divide(row, column, divide_south_of + 1, width)
        divide(row + divide_south_of+1, column,
               height-divide_south_of-1, width)

    def divide_vertically(row: int, column: int, height: int, width: int) -> None:
        divide_east_of = randrange(width - 1)
        passage_at = randrange(height)

        for y in range(height):
            if y == passage_at:
                continue

            cell = grid(row+y, column+divide_east_of)
            cell.unlink(cell.east)

        divide(row, column, height, divide_east_of+1)
        divide(row, column+divide_east_of+1, height, width-divide_east_of-1)

    for cell in grid.cell_list():
        for n in cell.neighbors():
            cell.link(n, False)

    divide(0, 0, grid.rows, grid.columns)

    return grid

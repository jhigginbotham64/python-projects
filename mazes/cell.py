from __future__ import annotations

from mazes.distances import *

from typing import Optional, List, Dict, TYPE_CHECKING
if TYPE_CHECKING:
    from mazes.grid import *

from queue import PriorityQueue


class Cell:

    row: int
    column: int
    links: Dict[Cell, bool]
    north: Optional[Cell]
    south: Optional[Cell]
    east: Optional[Cell]
    west: Optional[Cell]

    # may want to consider rewriting some of these functions as properties

    def __init__(self, row: int, column: int) -> None:
        self.row = row
        self.column = column
        self.links = {}

        self.north = None
        self.south = None
        self.east = None
        self.west = None

    # allows unpacking as pair of integers, which is how
    # Grid.__contains__ is implemented
    def __iter__(self) -> Iterable[int]:
        yield self.row
        yield self.column

    def link(self, cell: Cell, bidi: bool = True) -> Cell:
        self.links[cell] = True
        if bidi:
            cell.link(self, False)
        return self

    def unlink(self, cell: Cell, bidi: bool = True) -> Cell:
        del self.links[cell]
        if bidi:
            cell.unlink(self, False)
        return self

    def linked_cells(self) -> List[Cell]:
        return list(self.links.keys())

    def is_linked_to(self, cell: Cell) -> bool:
        return cell in self.links

    def neighbors(self) -> List[Cell]:
        l = []
        if self.north:
            l.append(self.north)
        if self.south:
            l.append(self.south)
        if self.east:
            l.append(self.east)
        if self.west:
            l.append(self.west)
        return l

    # the book uses "distances", i prefer to
    # use the name of the algorithm being called
    def dijkstra(self) -> Distances:
        # maps objects to integers representing distance
        distances = Distances(self)
        frontier = [self]

        while frontier:
            new_frontier = []

            for c in frontier:
                # d as in "from c to d", the "links" are cells here rather than edges
                for d in c.links:
                    if d in distances:
                        continue
                    distances[d] = distances[c] + 1
                    new_frontier.append(d)
            frontier = new_frontier

        return distances


class PolarCell(Cell):
    links: Dict[PolarCell, bool]
    cells: List[List[PolarCell]]
    cw: Optional[PolarCell]
    ccw: Optional[PolarCell]
    inward: Optional[PolarCell]
    outward: List[PolarCell]

    def __init__(self, row: int, column: int) -> None:
        # TODO: fix to where PolarCell does not inherit Cell.{north,south,east,west},
        # and do similarly for other Cell subclasses as respects their individual needs.
        super().__init__(row, column)
        self.cw = None
        self.ccw = None
        self.inward = None
        self.outward = []

    def neighbors(self) -> List[Cell]:
        l = []

        if self.cw:
            l.append(self.cw)
        if self.ccw:
            l.append(self.ccw)
        if self.inward:
            l.append(self.inward)
        l += self.outward

        return l


class HexCell(Cell):
    links: Dict[HexCell, bool]
    north: Optional[HexCell]
    south: Optional[HexCell]
    northeast: Optional[HexCell]
    southeast: Optional[HexCell]
    northwest: Optional[HexCell]
    southwest: Optional[HexCell]

    def __init__(self, row: int, column: int) -> None:
        super().__init__(row, column)
        self.north = None
        self.south = None
        self.northeast = None
        self.southeast = None
        self.northwest = None
        self.southwest = None

    def neighbors(self) -> List[HexCell]:
        l = []
        if self.northwest:
            l.append(self.northwest)
        if self.north:
            l.append(self.north)
        if self.northeast:
            l.append(self.northeast)
        if self.southwest:
            l.append(self.southwest)
        if self.south:
            l.append(self.south)
        if self.southeast:
            l.append(self.southeast)

        return l


class TriangleCell(Cell):
    def upright(self) -> bool:
        return ((self.row + self.column) % 2) == 0

    def neighbors(self) -> List[TriangleCell]:
        l = []
        if self.west:
            l.append(self.west)
        if self.east:
            l.append(self.east)
        if self.north and not self.upright():
            l.append(self.north)
        if self.south and self.upright():
            l.append(self.south)

        return l


class WeightedCell(Cell):
    weight: int

    # fun story: Python considers __init__ overridden regardless
    # of argument types and leaves you to call the superclass
    # constructor explicitly in all cases. joy.
    def __init__(self, row: int, column: int, weight: int = 1) -> None:
        super().__init__(row, column)
        self.weight = weight

    def __lt__(self, other: WeightedCell) -> bool:
        if not isinstance(other, Cell):
            return NotImplemented
        return self.weight < other.weight

    def dijkstra(self) -> Distances:
        weights = Distances(self)
        pending = PriorityQueue()
        pending.put(self)

        while not pending.empty():
            cell = pending.get()  # lowest-valued items first, fittingly
            for neighbor in cell.linked_cells():  # gotta keep Cell.linked_cells and Cell.neighbors straight
                # gotta say the different quantities used in the next few lines can
                # be super confusing to keep straight, so let's run them down:
                # - neighbor.weight is the weight of the neighboring cell being considered
                # - weights[cell] is distance of the current cell from the root
                # - weights[neighbor] is the distance of the neighbor from the root
                total_weight = (
                    weights[cell] if weights[cell] else 0) + neighbor.weight
                if neighbor not in weights or weights[neighbor] is None or total_weight < weights[neighbor]:
                    pending.put(neighbor)
                    weights[neighbor] = total_weight

        return weights


class OverCell(Cell):
    grid: WeaveGrid

    def __init__(self, row: int, column: int, grid: WeaveGrid) -> None:
        super().__init__(row, column)
        self.grid = grid

    def neighbors(self):
        l = super().neighbors()
        if self.can_tunnel_north():
            l.append(self.north.north)
        if self.can_tunnel_south():
            l.append(self.south.south)
        if self.can_tunnel_east():
            l.append(self.east.east)
        if self.can_tunnel_west():
            l.append(self.west.west)

        return l

    def can_tunnel_north(self) -> bool:
        return self.north and self.north.north and self.north.horizontal_passage()

    def can_tunnel_south(self) -> bool:
        return self.south and self.south.south and self.south.horizontal_passage()

    def can_tunnel_east(self) -> bool:
        return self.east and self.east.east and self.east.vertical_passage()

    def can_tunnel_west(self) -> bool:
        return self.west and self.west.west and self.west.vertical_passage()

    def horizontal_passage(self) -> bool:
        return self.is_linked_to(self.east) and self.is_linked_to(self.west) and not self.is_linked_to(self.north) and not self.is_linked_to(self.south)

    def vertical_passage(self) -> bool:
        return self.is_linked_to(self.north) and self.is_linked_to(self.south) and not self.is_linked_to(self.east) and not self.is_linked_to(self.west)

    def link(self, cell: Cell, bidi: bool = True) -> None:
        neighbor = None
        if self.north and self.north == cell.south:
            neighbor = self.north
        elif self.south and self.south == cell.north:
            neighbor = self.south
        elif self.east and self.east == cell.west:
            neighbor = self.east
        elif self.west and self.west == cell.west:
            neighbor = self.west

        if neighbor:
            self.grid.tunnel_under(neighbor)
        else:
            super().link(cell, bidi)


class UnderCell(Cell):

    def __init__(self, over_cell: OverCell) -> None:
        super().__init__(over_cell.row, over_cell.column)

        if over_cell.horizontal_passage():
            self.north = over_cell.north
            over_cell.north.south = self
            self.south = over_cell.south
            over_cell.south.north = self
            self.link(self.north)
            self.link(self.south)
        elif over_cell.vertical_passage():
            self.east = over_cell.east
            over_cell.east.west = self
            self.west = over_cell.west
            over_cell.west.east = self
            self.link(self.east)
            self.link(self.west)

    def horizontal_passage(self) -> bool:
        return bool(self.east or self.west)

    def vertical_passage(self) -> bool:
        return bool(self.north or self.south)


# NOTE repetition in this class indicates need for refactor
class SimpleOverCell(OverCell):
    def neighbors(self) -> List[SimpleOverCell]:
        l = []
        if self.north:
            l.append(self.north)
        if self.south:
            l.append(self.south)
        if self.east:
            l.append(self.east)
        if self.west:
            l.append(self.west)
        return l


class Cell3D(Cell):
    level: int
    up: Optional[Cell3D]
    down: Optional[Cell3D]

    def __init__(self, level: int, row: int, column: int) -> None:
        self.level = level
        self.up = None
        self.down = None
        super().__init__(row, column)

    def __iter__(self) -> Iterable[int]:
        yield self.level
        yield self.row
        yield self.column

    def neighbors(self) -> List[Cell3D]:
        l = super().neighbors()
        if self.up:
            l.append(self.up)
        if self.down:
            l.append(self.down)

        return l


class CubeCell(Cell):
    face: int

    def __init__(self, face: int, row: int, column: int) -> None:
        self.face = face
        super().__init__(row, column)

    def __iter__(self) -> Iterable[int]:
        yield self.face
        yield self.row
        yield self.column


class HemisphereCell(PolarCell):
    hemisphere: int

    def __init__(self, hemisphere: int, row: int, column: int) -> None:
        self.hemisphere = hemisphere
        super().__init__(row, column)

    def __iter__(self) -> Iterable[int]:
        yield self.hemisphere
        yield self.row
        yield self.column

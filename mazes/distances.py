
# https://stackoverflow.com/a/39757388
from __future__ import annotations
from typing import Optional, Tuple, Dict, Iterator, ItemsView, TYPE_CHECKING
if TYPE_CHECKING:
    from mazes.cell import *

# NOTE: enforcing that cells actually be of type Cell is not easy
# to do within the current structure of the mazes module. given
# the nature and function of the Distances class it seems that the
# most proper solution would be to refactor it to use generics.
# the only function that would be affected in any respect besides
# type hints would be Distances.path_to, which requires Cell.links
# or Cell.linked_cells. Since that aspect of Cell only indicates
# "something compatible with distance measurement" so far as this
# class is concerned, it could be refactored into an interface or mixin.


class Distances:
    root: Cell
    cells: Dict[Cell, int]

    def __init__(self, root: Cell) -> None:
        self.root = root
        self.cells = {root: 0}

    def __getitem__(self, key: Cell) -> Optional[int]:
        if key not in self:  # punt to __contains__
            raise KeyError(f"{str(key)}")
        return self.cells.get(key)

    def __setitem__(self, key: Cell, value: int) -> None:
        self.cells[key] = value

    def __delitem__(self, key: Cell) -> None:
        self.cells.pop(key, None)

    def __contains__(self, key: Cell) -> bool:
        return key in self.cells

    def __iter__(self) -> Iterator[Cell]:
        for c in self.cells:
            yield c

    def __len__(self) -> int:
        return len(self.cells)

    def items(self) -> ItemsView[Cell, int]:
        return self.cells.items()

    def path_to(self, goal: Cell) -> Distances:
        current = goal

        breadcrumbs = Distances(self.root)
        breadcrumbs[current] = self[current]

        while current is not self.root:
            for c in current.links:
                if self[c] < self[current]:
                    breadcrumbs[c] = self[c]
                    current = c
                    break

        return breadcrumbs

    def max(self) -> Tuple[Cell, int]:
        max_distance = 0
        max_cell = self.root

        for c, d in self.items():
            if d > max_distance:
                max_cell = c
                max_distance = d

        return max_cell, max_distance

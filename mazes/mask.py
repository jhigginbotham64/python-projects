# https://stackoverflow.com/a/39757388
from __future__ import annotations
import random
from typing import Optional, Tuple, Iterator, List
from PIL import Image, ImageColor


class Mask:
    rows: int
    columns: int
    bits: List[List[int]]

    def __init__(self, rows: int, columns: int) -> None:
        self.rows, self.columns = rows, columns
        self.bits = [[True for c in range(columns)] for r in range(rows)]

    # copy-pasted from Grid.__call__ with modifications
    # for different underlying metaphor and data type
    def __call__(self, row: int, column: int) -> bool:
        if row < 0 or row > self.rows - 1:
            return False
        if column < 0 or column > len(self.bits[row]) - 1:
            return False
        return self.bits[row][column]

    def count(self) -> int:
        """
          number of locations enabled in the mask, Python
          makes it easy by treating True/False as 1/0
        """
        return sum([sum(r) for r in self.bits])

    def enabled_indexes(self) -> Iterator[Tuple[int, int]]:
        """ iterate over enabled locations """
        for r in range(self.rows):
            for c in range(self.columns):
                if self(r, c):
                    yield r, c

    def random_location(self) -> Optional[Tuple[int, int]]:
        enabled_list = list(self.enabled_indexes())
        if enabled_list:
            return random.choice(enabled_list)
        else:
            return None

    @staticmethod
    def from_txt(fname: str) -> Mask:
        """ 
            takes name of mask file, which is a txt file
            consisting of X, ., and newlines, where X indicates
            "off" and . indicates "on", returns a new Mask.
            ...more technically, . indicates "on" and anything
            else indicates "off", except blank lines and 
            leading/trailing whitespace which indicate nothing.
        """
        mrows = []
        with open(fname) as mfile:  # mask file
            for line in mfile:  # python rocks
                l = line.strip()
                if l:  # ignore whitespace-only lines
                    mrows.append(
                        [True if c == '.' else False for c in list(l)])

        # handle "ragged rows" by padding with X.
        # this combined with the earlier whitespace
        # stripping has the effect of "left-justifying"
        # the mask definition, trading intuitiveness
        # for simplicity and a bit of robustness.
        max_row_len = max([len(r) for r in mrows])
        for r in mrows:
            diff = max_row_len - len(r)
            # whoops, not actually X because conversion has already
            # taken place, at this point X is a figure of speech.
            xpad = [False]*diff
            r += xpad

        rows, cols = len(mrows), max_row_len

        mask = Mask(rows, cols)
        mask.bits = mrows

        return mask

    @staticmethod
    def from_img(fname: str) -> Mask:
        """
            takes name of mask file, which is an image file
            consisting of...pixels (it's an image file) where
            black indicates "off" and anything else indicates "on".
        """
        # NOTE vs code isn't autocompleting for PIL.Image
        # instances, although it does for the module itself.
        # this may be due to lack of type hints.
        with Image.open(fname) as img:
            if img.mode != 'RGB':
                raise NotImplementedError(
                    f"image file mode {img.mode} is not supported, currently only RGB is supported")
            mask = Mask(img.height, img.width)
            for r in range(mask.rows):
                for c in range(mask.columns):
                    # the following line is the motivation for raising
                    # a NotImplementedError earlier.
                    # also note that Image.getpixel expects xy coordinates
                    # while Mask (and Grid) internally use yx.
                    if img.getpixel((c, r)) == ImageColor.getrgb("black"):
                        mask.bits[r][c] = False
                    else:
                        mask.bits[r][c] = True

            return mask

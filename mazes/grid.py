import math
from random import choice, randrange, random as rand
from typing import Optional, Tuple, List, Iterator, Iterable, Union

from PIL import Image, ImageDraw

from util import *
from mazes.cell import *
from mazes.distances import *
from mazes.mask import *


background_color = "white"
wall_color = "black"
arrow_color = "red"
outline_color = "grey"

img_mode = "RGB"

BACKGROUNDS = "backgrounds"
WALLS = "walls"
DISTANCES = "distances"

render_passes = [BACKGROUNDS, WALLS, DISTANCES]

DEFAULT_CELL_PX = 20
DEFAULT_INSET = 0.0
DEFAULT_WEAVE_GRID_INSET = 0.1


class Grid:

    rows: int
    columns: int
    cells: List[List[Cell]]

    def __init__(self, rows: int = 1, columns: int = 1) -> None:
        """
            create a grid with a given number of rows and columns
        """
        self.rows = rows
        self.columns = columns
        self.prepare_grid()
        self.configure_cells()

    # allowing membership tests using both tuples and cells
    # may not be the best complement to access only via
    # discrete positional arguments.
    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        # you can unpack a Cell because Cell.__iter__ yields its row and column in turn
        row, col, *_ = key
        if row < 0 or row > self.rows - 1:
            return False
        if col < 0 or col > len(self.cells[row]) - 1:
            return False
        if isinstance(key, Cell) and self.cells[row][col] != key:
            return False
        return True

    def __call__(self, row: int, column: int) -> Optional[Cell]:
        if (row, column) in self:
            return self.cells[row][column]

    def prepare_grid(self) -> None:
        """
            create initial cells.

            cells is a list of rows, which are lists of columns.
            you'd think that a field this important would be
            dealt with inside __init__, and technically it is,
            however putting it inside this function allows subclasses
            such as PolarGrid to handle it themselves rather than
            having to clobber or finagle an existing value.
        """
        self.cells = [[Cell(r, c) for c in range(self.columns)]
                      for r in range(self.rows)]

    def configure_cells(self) -> None:
        """
            create initial cell connections.

            as with cell creation, having this inside a function and simply
            calling that function within __init__ make OOP much simpler.
        """
        for cell in self.cell_list():
            if cell:  # can be None with MaskedGrid, in which case skip
                row, col = cell.row, cell.column

                cell.north = self(row - 1, col)
                cell.south = self(row + 1, col)
                cell.west = self(row, col - 1)
                cell.east = self(row, col + 1)

    def cell_list(self) -> Iterator[Cell]:
        for r in self.cells:
            for c in r:
                yield c

    def row_list(self) -> Iterator[List[Cell]]:
        for r in self.cells:
            yield r

    def random_cell(self) -> Cell:
        return self(randrange(self.rows), randrange(self.columns))

    def size(self) -> int:
        return self.rows * self.columns

    def deadends(self) -> List[Cell]:
        return [c for c in self.cell_list() if len(c.linked_cells()) == 1]

    # QUESTION: how to customize the behavior of this function?
    # current behavior prefers other deadends, and since this tends
    # to remove 2 deadends in 1 iteration, it tends to overshoot p.
    # it sometimes also creates "rooms", which we may or may not want.
    def braid(self, p: float = 1.0) -> None:
        des = self.deadends()
        for de in shuffle(des):
            if len(de.linked_cells()) != 1 or rand() > p:
                continue

            # only look at unliked cells
            neighbors = [c for c in de.neighbors() if not c.is_linked_to(de)]
            # prefer other deadends which have not yet been braided by this function
            best = [c for c in neighbors if len(c.linked_cells()) == 1]
            if not best:
                best = neighbors

            neighbor = choice(best)
            de.link(neighbor)

    def dijkstra(self, start: Optional[Iterable[int]] = None) -> Distances:
        if start not in self:
            # this default wouldn't normally make sense, except it gets used a lot.
            # kinda odd given that path offers no such conveniences but whatever.
            start = self(round(self.rows / 2), round(self.columns / 2))
        if not isinstance(start, Cell):
            row, col, *_ = start
            start = self(row, col)

        self.distances = start.dijkstra()
        return self.distances

    def path(self, from_cell: Optional[Iterable[int]] = None, to_cell: Optional[Iterable[int]] = None) -> Optional[Distances]:
        if from_cell not in self or to_cell not in self:
            return None
        if not isinstance(from_cell, Cell):
            row, col, *_ = from_cell
            from_cell = self(row, col)
        if not isinstance(to_cell, Cell):
            row, col, *_ = to_cell
            to_cell = self(row, col)

        dist_from = from_cell.dijkstra()
        self.distances = dist_from.path_to(to_cell)
        return self.distances

    def contents_of(self, cell: Cell) -> str:
        """ overrides of this function should always return a string """
        return " "

    def __str__(self) -> str:
        out = "+" + "---+" * self.columns + "\n"

        for r in self.cells:
            top = "|"
            bottom = "+"
            for c in r:
                if not c:
                    c = Cell(-1, -1)

                # 3 spaces. Three shall be the number of the counting and the number of the counting shall be three. Four shalt thou not count, neither shalt thou count two, excepting that thou then proceedeth to three. Five is right out.
                body = f" {self.contents_of(c)} "
                east_boundary = " " if c.is_linked_to(c.east) else "|"
                top += body + east_boundary

                south_boundary = "   " if c.is_linked_to(c.south) else "---"
                corner = "+"
                bottom += south_boundary + corner

            out += top + "\n"
            out += bottom + "\n"

        return out

    def background_color_for(self, cell: Cell) -> Optional[Tuple[int, int, int]]:
        """ overrides of this function should always return a Pillow color """
        return None

    def as_image(self, cell_px: int = DEFAULT_CELL_PX, inset: float = DEFAULT_INSET) -> Image:
        img_size = (cell_px * self.columns + 1, cell_px * self.rows + 1)
        inset = int(cell_px * inset)

        img = Image.new(img_mode, img_size, background_color)

        for mode in render_passes:
            for cell in self.cell_list():
                x = cell.column * cell_px
                y = cell.row * cell_px

                x1 = cell.column * cell_px
                x2 = (cell.column + 1) * cell_px
                y1 = cell.row * cell_px
                y2 = (cell.row + 1) * cell_px

                d = ImageDraw.Draw(img)

                if inset > 0:
                    self.draw_cell_with_inset(
                        d, cell, mode, cell_px, wall_color, x, y, inset)
                else:
                    self.draw_cell_without_inset(
                        d, cell, mode, cell_px, wall_color, x, y)

        return img

    def draw_cell_without_inset(self, d: ImageDraw, cell: Cell, mode: str, cell_px: int, wall_color: str, x: int, y: int) -> None:
        x1, y1 = x, y
        x2 = x1 + cell_px
        y2 = y1 + cell_px

        if mode == BACKGROUNDS:
            color = self.background_color_for(cell)  # used by ColoredGrid
            if color is not None:
                d.rectangle([x1, y1, x2, y2], color, color)
        elif mode == WALLS:
            if cell.north is None:
                d.line([x1, y1, x2, y1], wall_color)
            if cell.west is None:
                d.line([x1, y1, x1, y2], wall_color)
            if not cell.is_linked_to(cell.east):
                d.line([x2, y1, x2, y2], wall_color)
            if not cell.is_linked_to(cell.south):
                d.line([x1, y2, x2, y2], wall_color)
        elif mode == DISTANCES:
            txt = self.contents_of(cell)  # used by DistanceGrid
            tx, ty = d.getfont().getsize(txt)
            d.text((x1 + cell_px / 2 - tx / 2, y1 +
                    cell_px / 2 - ty / 2), txt, fill=(0, 0, 0))

    def cell_coordinates_with_inset(self, x: int, y: int, cell_px: int, inset: int) -> Tuple[int, int, int, int, int, int, int, int]:
        x1, x4 = x, x + cell_px
        x2 = x1 + inset
        x3 = x4 - inset

        y1, y4 = y, y + cell_px
        y2 = y1 + inset
        y3 = y4 - inset

        return (x1, y1, x2, y2, x3, y3, x4, y4)

    def draw_cell_with_inset(self, d: ImageDraw, cell: Cell, mode: str, cell_px: int, wall_color: str, x: int, y: int, inset: int) -> None:
        x1, y1, x2, y2, x3, y3, x4, y4 = self.cell_coordinates_with_inset(
            x, y, cell_px, inset)

        if mode == BACKGROUNDS:
            # color = self.background_color_for(
            #     cell)  # used by ColoredGrid
            # if color is not None:
            #     d.rectangle([x1, y1, x2, y2], color, color)
            pass
        elif mode == WALLS:

            if cell.is_linked_to(cell.north):
                d.line([x2, y1, x2, y2], wall_color)
                d.line([x3, y1, x3, y2], wall_color)
            else:
                d.line([x2, y2, x3, y2], wall_color)

            if cell.is_linked_to(cell.south):
                d.line([x2, y3, x2, y4], wall_color)
                d.line([x3, y3, x3, y4], wall_color)
            else:
                d.line([x2, y3, x3, y3], wall_color)

            if cell.is_linked_to(cell.west):
                d.line([x1, y2, x2, y2], wall_color)
                d.line([x1, y3, x2, y3], wall_color)
            else:
                d.line([x2, y2, x2, y3], wall_color)

            if cell.is_linked_to(cell.east):
                d.line([x3, y2, x4, y2], wall_color)
                d.line([x3, y3, x4, y3], wall_color)
            else:
                d.line([x3, y2, x3, y3], wall_color)

        elif mode == DISTANCES:
            # txt = self.contents_of(cell)  # used by DistanceGrid
            # tx, ty = d.getfont().getsize(txt)
            # d.text((x1 + cell_px / 2 - tx / 2, y1 +
            #         cell_px / 2 - ty / 2), txt, fill=(0, 0, 0))
            pass


class DistanceGrid(Grid):
    def contents_of(self, cell: Cell) -> str:
        if hasattr(self, "distances") and cell in self.distances:
            return n_to_base_str(self.distances[cell], 36)
        else:
            return super().contents_of(cell)


class ColoredGrid(Grid):

    _distances: Optional[Distances]

    @property
    def distances(self) -> Distances:
        return self._distances

    @distances.setter
    def distances(self, value: Distances) -> None:
        farthest, self.maximum = value.max()
        self._distances = value

    def background_color_for(self, cell: Cell) -> Optional[Tuple[int, int, int]]:
        # paranoia
        if hasattr(self, "_distances") and self.distances and cell in self.distances:
            distance = self.distances[cell]
            if not distance:
                return None
            intensity = float(self.maximum - distance) / self.maximum
            dark = round(255 * intensity)
            bright = round(128 + (127 * intensity))
            return (dark, bright, dark)


class ColoredDistanceGrid(DistanceGrid, ColoredGrid):
    pass


class MaskedGrid(Grid):

    mask: Mask

    def __init__(self, mask: Mask) -> None:
        self.mask = mask
        # usually one would call the superclass initializer first,
        # but in this case overridden methods are used in the
        # initializer which depend on the mask
        super().__init__(mask.rows, mask.columns)

    def prepare_grid(self) -> None:
        self.cells = [[Cell(r, c) if self.mask(r, c) else None
                       for c in range(self.columns)]
                      for r in range(self.rows)]

    def random_cell(self) -> Optional[Cell]:
        loc = self.mask.random_location()
        if loc:
            r, c = loc
            return self(r, c)
        else:
            return None

    def cell_list(self) -> Iterator[Cell]:
        for r, c in self.mask.enabled_indexes():
            yield self(r, c)

    def size(self) -> int:
        return self.mask.count()


class PolarGrid(Grid):

    def __init__(self, rows: int = 1) -> None:
        super().__init__(rows)

    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        row, *_ = key
        if row < 0 or row > self.rows - 1:
            return False
        return True

    def __call__(self, row: int, column: int) -> Optional[PolarCell]:
        # other grids so far do not have "wrap-around adjacency".
        # in case i decide that this is how to implement actual
        # wrap-around in the future, it's worth noting that this
        # is technically horizontal wrap even though the final effect
        # is "polar wrap" or "radiality" or something like that.
        # also it seems really strange to require a row and column
        # to test membership when the columns automatically wrap,
        # but whatever.
        if (row, column) in self:
            return self.cells[row][column % len(self.cells[row])]

    # NOTE with the similarity of Grid and MaskedGrid with respect
    # to prepare_grid, i sense a possibility of a SquareGrid class
    # with Grid being abstract and MaskedGrid being a mixin. or an
    # abstract base...for mixins. or something.
    def prepare_grid(self) -> None:
        self.cells = []

        row_height = 1.0 / self.rows
        self.cells.append([PolarCell(0, 0)])  # center

        for r in range(1, self.rows):
            radius = float(r) / self.rows
            circumference = 2 * math.pi * radius

            previous_count = len(self.cells[r-1])
            est_cell_width = circumference / previous_count
            ratio = round(est_cell_width / row_height)

            cells_in_new_row = previous_count * ratio
            self.cells.append([PolarCell(r, c)
                               for c in range(cells_in_new_row)])

    def configure_cells(self) -> None:
        # TODO mop up my local variables to be more consistent
        # about how i use r, c, and cell, because it's confusing
        for cell in self.cell_list():
            row, col = cell.row, cell.column
            if row > 0:
                cell.cw = self(row, col + 1)
                cell.ccw = self(row, col - 1)

                ratio = len(self.cells[row]) / len(self.cells[row-1])
                parent = self.cells[row-1][int(col / ratio)]
                parent.outward.append(cell)
                cell.inward = parent

    def random_cell(self) -> Optional[PolarCell]:
        """
            return a random cell.

            trades speed for randomness by aggregating all cells
            and choosing uniformly. the faster method of generating
            a random row and then a random column based on the
            length of the row is biased towards the less-numerous
            cells in smaller rows, yes i could break out the math
            to prove it.
        """
        l = list(self.cell_list())
        if l:
            return choice(l)
        else:
            return None

    def as_image(self, cell_px: int = DEFAULT_CELL_PX) -> Image:
        img_size = 2 * self.rows * cell_px

        img = Image.new(img_mode, (img_size + 1, img_size + 1),
                        background_color)
        center = img_size / 2

        d = ImageDraw.Draw(img)

        # NOTE VS code thinks c is NoneType despite type hints on cell_list.
        # looking back it doesn't seem to understand Optional[Cell] either
        # (i.e. for Grid.__call__).
        # NOTE 8/7/2020 c is now correctly identified as a PolarCell, but it
        # is not identified as Cell inside Grid methods. curious that the
        # seemingly more complex case would be handled properly.
        # NOTE looking at the editor hints has made me realize that i need
        # to be on the lookout for places where i accidentally use Cell
        # instead of PolarCell.
        for c in self.cell_list():
            if c.row == 0:
                continue

            t = 2 * math.pi / len(self.cells[c.row])  # theta
            inr = c.row * cell_px  # inner radius
            our = (c.row + 1) * cell_px  # outer radius
            t_ccw = c.column * t  # angle of counter clockwise boundary
            t_cw = (c.column + 1) * t  # angle of clockwise boundary

            # ax = center + round((inr * math.cos(t_ccw)))
            # ay = center + round((inr * math.sin(t_ccw)))
            # bx = center + round((our * math.cos(t_ccw)))
            # by = center + round((our * math.sin(t_ccw)))
            cx = center + round((inr * math.cos(t_cw)))
            cy = center + round((inr * math.sin(t_cw)))
            dx = center + round((our * math.cos(t_cw)))
            dy = center + round((our * math.sin(t_cw)))

            if not c.is_linked_to(c.inward):
                # Pillow defines arcs as "ellipse segments" or
                # something like that, where the ellipse is
                # defined by the bounding box and the angles
                # define the segment of the ellipse that gets drawn.
                # here the bounding box is a square with side length
                # equal to two times the inner radius of the current
                # cell, with corners defined relative to the center of
                # of the polar grid.
                bb_x_1 = center - inr
                bb_y_1 = center - inr
                bb_x_2 = center + inr
                bb_y_2 = center + inr
                d.arc([bb_x_1, bb_y_1, bb_x_2, bb_y_2], math.degrees(
                    t_ccw), math.degrees(t_cw), wall_color)
            if not c.is_linked_to(c.cw):
                d.line([cx, cy, dx, dy], wall_color)

        d.ellipse([0, 0, img_size, img_size], fill=None, outline=wall_color)

        return img


class HexGrid(Grid):
    def prepare_grid(self) -> None:
        self.cells = [[HexCell(r, c) for c in range(self.columns)]
                      for r in range(self.rows)]

    def configure_cells(self) -> None:
        for cell in self.cell_list():
            row, col = cell.row, cell.column

            even = (col % 2) == 0
            north_diagonal = row - 1 if even else row
            south_diagonal = row if even else row + 1

            cell.northwest = self(north_diagonal, col - 1)
            cell.north = self(row - 1, col)
            cell.northeast = self(north_diagonal, col + 1)
            cell.southwest = self(south_diagonal, col - 1)
            cell.south = self(row + 1, col)
            cell.southeast = self(south_diagonal, col + 1)

    def as_image(self, cell_px: int = DEFAULT_CELL_PX) -> Image:
        apx = cell_px / 2.0
        bpx = cell_px * math.sqrt(3.0) / 2.0
        w = cell_px * 2
        h = bpx * 2

        img_w = int(3 * apx * self.columns + apx + 0.5)
        img_h = int(h * self.rows + bpx + 0.5)

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        for mode in render_passes:
            for cell in self.cell_list():
                cx = cell_px + 3 * cell.column * apx
                cy = (bpx + cell.row * h) + \
                    (bpx if (cell.column % 2) != 0 else 0)

                # f/n = far/near
                # n/s/e/w = north/south/east/west
                x_fw = int(cx - cell_px)
                x_nw = int(cx - apx)
                x_ne = int(cx + apx)
                x_fe = int(cx + cell_px)

                # m = middle
                y_n = int(cy - bpx)
                y_m = int(cy)
                y_s = int(cy + bpx)

                if mode == BACKGROUNDS:
                    color = self.background_color_for(cell)
                    if color:
                        points = [
                            x_fw, y_m, x_nw, y_n, x_ne, y_n,
                            x_fe, y_m, x_ne, y_s, x_nw, y_s
                        ]
                        d.polygon(points, color, color)

                elif mode == DISTANCES:
                    if cell.southwest is None:
                        d.line([x_fw, y_m, x_nw, y_s], wall_color)
                    if cell.northwest is None:
                        d.line([x_fw, y_m, x_nw, y_n], wall_color)
                    if cell.north is None:
                        d.line([x_nw, y_n, x_ne, y_n], wall_color)
                    if not cell.is_linked_to(cell.northeast):
                        d.line([x_ne, y_n, x_fe, y_m], wall_color)
                    if not cell.is_linked_to(cell.southeast):
                        d.line([x_fe, y_m, x_ne, y_s], wall_color)
                    if not cell.is_linked_to(cell.south):
                        d.line([x_ne, y_s, x_nw, y_s], wall_color)

        return img


class TriangleGrid(Grid):
    def prepare_grid(self) -> None:
        self.cells = [[TriangleCell(r, c)
                       for c in range(self.columns)]
                      for r in range(self.rows)]

    def configure_cells(self) -> None:
        for cell in self.cell_list():
            row, col = cell.row, cell.column

            cell.west = self(row, col - 1)
            cell.east = self(row, col + 1)

            if cell.upright():
                cell.south = self(row + 1, col)
            else:
                cell.north = self(row - 1, col)

    def as_image(self, cell_px: int = DEFAULT_CELL_PX) -> Image:
        half_w = cell_px / 2.0
        h = cell_px * math.sqrt(3) / 2.0
        half_h = h / 2.0

        img_w = int(cell_px * (self.columns + 1) / 2.0)
        img_h = int(h * self.rows)

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        for mode in render_passes:
            for cell in self.cell_list():
                cx = half_w + cell.column * half_w
                cy = half_h + cell.row * h

                west_x = int(cx - half_w)
                mid_x = int(cx)
                east_x = int(cx + half_w)

                apex_y = cy + half_h * (-1 if cell.upright() else 1)
                base_y = cy + half_h * (1 if cell.upright() else -1)

                if mode == BACKGROUNDS:
                    color = self.background_color_for(cell)
                    if color:
                        point = [
                            west_x, base_y,
                            mid_x, apex_y,
                            east_x, base_y
                        ]
                        d.polygon(points, color, color)

                elif mode == WALLS:
                    if not cell.west:
                        d.line([west_x, base_y, mid_x, apex_y], wall_color)

                    if not cell.is_linked_to(cell.east):
                        d.line([east_x, base_y, mid_x, apex_y], wall_color)

                    no_south = cell.upright() and not cell.south
                    not_linked = not cell.upright() and not cell.is_linked_to(cell.north)

                    if no_south or not_linked:
                        d.line([east_x, base_y, west_x, base_y], wall_color)

        return img


class WeightedGrid(ColoredDistanceGrid):
    def prepare_grid(self) -> None:
        self.cells = [[WeightedCell(r, c) for c in range(self.columns)]
                      for r in range(self.rows)]

    def background_color_for(self, cell: WeightedCell) -> Optional[Tuple[int, int, int]]:
        if cell.weight > 1:
            return (255, 0, 0)  # mono-red for all "obstacles"
        # NOTE conditions like this make Distance's implementation of
        # the sequence and container protocols look amateurish
        elif hasattr(self, "_distances") and self.distances and cell in self.distances:
            distance = self.distances[cell]
            if not distance:
                return None
            intensity = round(
                64 + 191 * (self.maximum - distance) / self.maximum)
            # yellow for normal squares instead of parent's green
            return (intensity, intensity, 0)


class WeaveGrid(Grid):
    under_cells = List[UnderCell]

    def __init__(self, rows: int, columns: int) -> None:
        self.under_cells = []
        super().__init__(rows, columns)

    def prepare_grid(self) -> None:
        self.cells = [[OverCell(r, c, self) for c in range(
            self.columns)] for r in range(self.rows)]

    def tunnel_under(self, over_cell: OverCell) -> None:
        self.under_cells.append(UnderCell(over_cell))

    def cell_list(self) -> Iterator[Cell]:
        yield from super().cell_list()  # i love python

        for c in self.under_cells:
            yield c

    def as_image(self, cell_px: int = DEFAULT_CELL_PX, inset: int = DEFAULT_WEAVE_GRID_INSET) -> Image:
        return super().as_image(cell_px, inset)

    def draw_cell_with_inset(self, d: ImageDraw, cell: Union[OverCell, UnderCell], mode: str, cell_px: int, wall_color: str, x: int, y: int, inset: int) -> None:
        if isinstance(cell, OverCell):
            super().draw_cell_with_inset(d, cell, mode, cell_px, wall_color, x, y, inset)
        elif isinstance(cell, UnderCell):
            x1, y1, x2, y2, x3, y3, x4, y4 = self.cell_coordinates_with_inset(
                x, y, cell_px, inset)

            if cell.vertical_passage():
                d.line([x2, y1, x2, y2], wall_color)
                d.line([x3, y1, x3, y2], wall_color)
                d.line([x2, y3, x2, y4], wall_color)
                d.line([x3, y3, x3, y4], wall_color)
            elif cell.horizontal_passage():
                d.line([x1, y2, x2, y2], wall_color)
                d.line([x1, y3, x2, y3], wall_color)
                d.line([x3, y2, x4, y2], wall_color)
                d.line([x3, y3, x4, y3], wall_color)


class PreconfiguredGrid(WeaveGrid):
    def prepare_grid(self) -> None:
        self.cells = [[SimpleOverCell(r, c, self) for c in range(
            self.columns)] for r in range(self.rows)]


class Grid3D(Grid):
    levels: int

    def __init__(self, levels: int, rows: int, columns: int) -> None:
        self.levels = levels
        super().__init__(rows, columns)

    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        lev, row, col, *_ = key
        if lev < 0 or lev > self.levels - 1:
            return False
        if row < 0 or row > len(self.cells[lev]) - 1:
            return False
        if col < 0 or col > len(self.cells[lev][row]) - 1:
            return False
        if isinstance(key, Cell) and self.cells[lev][row][col] != key:
            return False
        return True

    def __call__(self, level: int, row: int, column: int) -> Optional[Cell3D]:
        if (level, row, column) in self:
            return self.cells[level][row][column]

    def prepare_grid(self) -> None:
        self.cells = [[[Cell3D(l, r, c) for c in range(self.columns)]
                       for r in range(self.rows)] for l in range(self.levels)]

    def configure_cells(self) -> None:
        for cell in self.cell_list():
            l, r, c = cell.level, cell.row, cell.column

            cell.north = self(l, r-1, c)
            cell.south = self(l, r+1, c)
            cell.west = self(l, r, c-1)
            cell.east = self(l, r, c+1)
            cell.down = self(l-1, r, c)
            cell.up = self(l+1, r, c)

    def level_list(self) -> Iterator[List[List[Cell3D]]]:
        for l in self.cells:
            yield l

    def row_list(self) -> Iterator[List[Cell3D]]:
        for l in self.cells:
            for r in l:
                yield r

    def cell_list(self) -> Iterator[Cell3D]:
        for l in self.cells:
            for r in l:
                for c in r:
                    yield c

    def random_cell(self) -> Cell3D:
        return self(randrange(self.levels), randrange(self.rows), randrange(self.columns))

    def size(self) -> int:
        return self.levels * self.rows * self.columns

    def as_image(self, cell_px: int = 10, inset: float = 0.0, margin: float = 5) -> Image:
        inset = int(cell_px * inset)

        w = cell_px * self.columns
        h = cell_px * self.rows
        img_w = w * self.levels + (self.levels - 1) * margin
        img_h = h

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        for mode in render_passes:
            for cell in self.cell_list():
                x = cell.level * (w + margin) + cell.column * cell_px
                y = cell.row * cell_px

                if inset > 0:
                    self.draw_cell_with_inset(
                        d, cell, mode, cell_px, wall_color, x, y, inset)
                else:
                    self.draw_cell_without_inset(
                        d, cell, mode, cell_px, wall_color, x, y)

                if mode == WALLS:
                    mid_x = x + cell_px / 2
                    mid_y = y + cell_px / 2

                    if cell.is_linked_to(cell.down):
                        d.line([mid_x - 3, mid_y, mid_x -
                                1, mid_y + 2], arrow_color)
                        d.line([mid_x - 3, mid_y, mid_x -
                                1, mid_y - 2], arrow_color)
                    if cell.is_linked_to(cell.up):
                        d.line([mid_x + 3, mid_y, mid_x +
                                1, mid_y + 2], arrow_color)
                        d.line([mid_x + 3, mid_y, mid_x +
                                1, mid_y - 2], arrow_color)

        return img


class CylinderGrid(Grid):
    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        row, col, *_ = key
        if row < 0 or row > self.rows - 1:
            return False
        col = col % len(self.cells[row])
        if isinstance(key, Cell) and self.cells[row][col] != key:
            return False
        return True

    def __call__(self, row: int, column: int) -> Optional[Cell]:
        if (row, column) in self:
            return self.cells[row][column % len(self.cells[row])]


class MoebiusGrid(CylinderGrid):
    def __init__(self, rows: int, columns: int) -> None:
        super().__init__(rows, columns*2)

    def as_image(self, cell_px: int = 10, inset: float = 0.0) -> Image:
        h = cell_px * self.rows
        mid = round(self.columns / 2)

        img_w = cell_px * mid
        img_h = h * 2

        inset = int(cell_px * inset)

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        for mode in render_passes:
            for cell in self.cell_list():
                x = (cell.column % mid) * cell_px
                y = cell.row * cell_px

                if cell.column >= mid:
                    y += h

                if inset > 0:
                    self.draw_cell_with_inset(
                        d, cell, mode, cell_px, wall_color, x, y, inset)
                else:
                    self.draw_cell_without_inset(
                        d, cell, mode, cell_px, wall_color, x, y)

        return img


class CubeGrid(Grid):
    dim: int

    def __init__(self, dim: int) -> None:
        self.dim = dim
        super().__init__(dim, dim)

    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        face, row, col, *_ = key
        face, row, col = self.wrap(face, row, col)
        if face not in range(6):
            return False
        if row < 0 or row > len(self.cells[face]) - 1:
            return False
        if col < 0 or col > len(self.cells[face][row]) - 1:
            return False
        if isinstance(key, Cell) and self.cells[face][row][col] != key:
            return False
        return True

    def __call__(self, face: int, row: int, column: int) -> Optional[CubeCell]:
        if (face, row, column) in self:
            face, row, column = self.wrap(face, row, column)
            return self.cells[face][row][column]

    def wrap(self, face: int, row: int, column: int) -> Tuple[int, int, int]:
        n = self.dim-1
        if face in range(6):
            if row < 0:
                return [
                    (4, column, 0),
                    (4, n, column),
                    (4, n-column, n),
                    (4, 0, n-column),
                    (3, 0, n-column),
                    (1, n, column)
                ][face]
            elif row >= self.dim:
                return [
                    (5, n-column, 0),
                    (5, 0, column),
                    (5, column, n),
                    (5, n, n-column),
                    (1, 0, column),
                    (3, n, n-column)
                ][face]
            elif column < 0:
                return [
                    (3, row, n),
                    (0, row, n),
                    (1, row, n),
                    (2, row, n),
                    (0, 0, row),
                    (0, n, n-row)
                ][face]
            elif column >= self.dim:
                return [
                    (1, row, 0),
                    (2, row, 0),
                    (3, row, 0),
                    (0, row, 0),
                    (2, 0, n-row),
                    (2, n, row)
                ][face]
        return (face, row, column)

    def prepare_grid(self) -> None:
        self.cells = [[[CubeCell(f, r, c) for c in range(self.dim)]
                       for r in range(self.dim)] for f in range(6)]

    def configure_cells(self) -> None:
        for cell in self.cell_list():
            f, r, c = cell.face, cell.row, cell.column

            cell.west = self(f, r, c-1)
            cell.east = self(f, r, c+1)
            cell.north = self(f, r-1, c)
            cell.south = self(f, r+1, c)

    def face_list(self) -> Iterable[List[List[CubeCell]]]:
        for f in self.cells:
            yield f

    def row_list(self) -> Iterable[List[CubeCell]]:
        for f in self.cells:
            for r in f:
                yield r

    def cell_list(self) -> Iterable[CubeCell]:
        for f in self.cells:
            for r in f:
                for c in r:
                    yield c

    def random_cell(self) -> CubeCell:
        f = randrange(6)
        r = randrange(self.dim)
        c = randrange(self.dim)
        return self(f, r, c)

    def size(self) -> int:
        return 6 * self.dim * self.dim

    def draw_outlines(self, d: ImageDraw, h: int, w: int, color: str) -> None:
        d.rectangle([0, h, w, h*2], outline=color)
        d.rectangle([w*2, h, w, h*2], outline=color)
        d.line([w*3, h, w*3, h*2], color)
        d.rectangle([w, 0, w*2, h], outline=color)
        d.rectangle([w, h*2, w*2, h*3], outline=color)

    def as_image(self, cell_px: int = 10, inset: float = 0) -> Image:
        inset = int(cell_px * inset)

        fw = cell_px * self.dim
        fh = cell_px * self.dim

        img_w = 4 * fw
        img_h = 3 * fh

        offsets = [[0, 1], [1, 1], [2, 1], [3, 1], [1, 0], [1, 2]]

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        self.draw_outlines(d, fw, fh, outline_color)

        for mode in render_passes:
            for cell in self.cell_list():
                x = offsets[cell.face][0] * fw + cell.column * cell_px
                y = offsets[cell.face][1] * fh + cell.row * cell_px

                if inset > 0:
                    self.draw_cell_with_inset(
                        d, cell, mode, cell_px, wall_color, x, y, inset)
                else:
                    self.draw_cell_without_inset(
                        d, cell, mode, cell_px, wall_color, x, y)

        return img

    def draw_cell_without_inset(self, d: ImageDraw, cell: CubeCell, mode: str, cell_px: int, wall_color: str, x: int, y: int) -> None:
        x1, y1 = x, y
        x2 = x1 + cell_px
        y2 = y1 + cell_px

        if mode == BACKGROUNDS:
            color = self.background_color_for(cell)
            if color is not None:
                d.rectangle([x, y, x2, y2], color, color)
        elif mode == WALLS:
            if cell.north.face != cell.face and not cell.is_linked_to(cell.north):
                d.line([x1, y1, x2, y1], wall_color)

            if cell.west.face != cell.face and not cell.is_linked_to(cell.west):
                d.line([x1, y1, x1, y2], wall_color)

            if not cell.is_linked_to(cell.east):
                d.line([x2, y1, x2, y2], wall_color)
            if not cell.is_linked_to(cell.south):
                d.line([x1, y2, x2, y2], wall_color)


class HemisphereGrid(PolarGrid):
    id: int

    def __init__(self, id: int, rows: int) -> None:
        self.id = id
        super().__init__(rows)

    def size_of_row(self, row: int) -> int:
        return len(self.cells[row])

    def prepare_grid(self) -> None:
        self.cells = []

        angular_height = math.pi / (2 * self.rows)

        self.cells.append([HemisphereCell(self.id, 0, 0)])  # center

        for r in range(1, self.rows):
            theta = (r+1) * angular_height
            radius = math.sin(theta)
            circumference = 2 * math.pi * radius

            previous_count = len(self.cells[r-1])
            est_cell_width = circumference / previous_count
            ratio = round(est_cell_width / angular_height)

            cells_in_new_row = previous_count * ratio
            self.cells.append([HemisphereCell(self.id, r, c)
                               for c in range(cells_in_new_row)])


class SphereGrid(Grid):
    equator: int
    cells: List[HemisphereGrid]

    def __init__(self, rows: int) -> None:
        if rows % 2 != 0:
            raise NotImplementedError(
                "SphereGrid does not currently support odd numbers of rows")
        self.equator = int(rows / 2)
        super().__init__(rows, 1)

    def __contains__(self, key: Optional[Iterable[int]] = None) -> bool:
        if not key:
            return False
        hem, row, col, *_ = key
        if hem not in range(2):
            return False
        if row < 0 or row > self.cells[hem].rows - 1:
            return False
        if col < 0 or col > self.cells[hem].size_of_row(row) - 1:
            return False
        if isinstance(key, Cell) and self.cells[hem](row, col) != key:
            return False
        return True

    def __call__(self, hemisphere: int, row: int, column: int) -> Optional[HemisphereCell]:
        if (hemisphere, row, column) in self:
            return self.cells[hemisphere](row, column)

    def prepare_grid(self) -> None:
        self.cells = [HemisphereGrid(i, self.equator) for i in range(2)]

    def configure_cells(self) -> None:
        belt = self.equator - 1
        for i in range(self.size_of_row(belt)):
            a, b = self(0, belt, i), self(1, belt, i)
            a.outward.append(b)
            b.outward.append(a)

    def size_of_row(self, row: int) -> int:
        return self.cells[0].size_of_row(row)

    def cell_list(self) -> Iterable[HemisphereCell]:
        for hem in self.cells:
            for c in hem.cell_list():
                yield c

    def random_cell(self) -> HemisphereCell:
        return self.cells[randrange(2)].random_cell()

    def as_image(self, ideal_size: int = 10) -> Image:
        img_h = ideal_size * self.rows
        img_w = ideal_size * self.cells[0].size_of_row(self.equator - 1)

        img = Image.new(img_mode, (img_w + 1, img_h + 1), background_color)

        d = ImageDraw.Draw(img)

        for cell in self.cell_list():
            row_size = self.size_of_row(cell.row)
            cell_width = float(img_w) / row_size

            x1 = cell.column * cell_width
            x2 = x1 + cell_width

            y1 = cell.row * ideal_size
            y2 = y1 + ideal_size

            if cell.hemisphere > 0:
                y1 = img_h - y1
                y2 = img_h - y2

            x1, y1, x2, y2 = round(x1), round(y1), round(x2), round(y2)

            if cell.row > 0:
                if not cell.is_linked_to(cell.cw):
                    d.line([x2, y1, x2, y2], wall_color)
                if not cell.is_linked_to(cell.inward):
                    d.line([x1, y1, x2, y1], wall_color)

            if cell.hemisphere == 0 and cell.row == self.equator - 1:
                if not cell.is_linked_to(cell.outward[0]):
                    d.line([x1, y2, x2, y2], wall_color)

        return img

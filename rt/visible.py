from __future__ import annotations
import random
import string
import typing
import statistics

import numpy as np
from nptyping import NDArray, Float64
import matplotlib.pyplot as plt
import matplotlib.image as mpi
from PIL import Image, ImageDraw

import rt.numeric as rtn

import util


class Color(rtn.NpFourTuple):
    def __init__(self, r: Float64, g: Float64, b: Float64) -> None:
        # default w = 1.0 for "opaque", i'm not using alpha yet because book
        super().__init__(r, g, b, 1.0)

    def get_r(self) -> rtn.MepsFloat:
        return rtn.MepsFloat(self[0])

    def set_r(self, value: Float64) -> None:
        self[0] = value

    def get_g(self) -> rtn.MepsFloat:
        return rtn.MepsFloat(self[1])

    def set_g(self, value: Float64) -> None:
        self[1] = value

    def get_b(self) -> rtn.MepsFloat:
        return rtn.MepsFloat(self[2])

    def set_b(self, value: Float64) -> None:
        self[2] = value

    # def get_a(self) -> rtn.MepsFloat:
    #     return rtn.MepsFloat(self[3])

    # def set_a(self, value: Float64) -> None:
    #     self[3] = value

    r = property(get_r, set_r)
    g = property(get_g, set_g)
    b = property(get_b, set_b)
    # a = property(get_a, set_a)

    # aliases
    red = r
    green = g
    blue = b
    # alpha = a

    @property
    def scaled(self) -> Tuple[int, int, int]:
        # returns the color scaled and clipped to [0, 255],
        # as a tuple such as can be passed to Pillow.
        r = np.clip(round(self.r * 255), 0, 255)
        g = np.clip(round(self.g * 255), 0, 255)
        b = np.clip(round(self.b * 255), 0, 255)
        return (r, g, b)

    @property
    def bgr(self) -> Color:
        # https://stackoverflow.com/questions/39316447/opencv-giving-wrong-color-to-colored-images-on-loading#39316695
        # https://answers.opencv.org/question/133453/how-does-imshow-know-the-color-map-of-the-image/
        return Color(self.b, self.g, self.r)

    # converting to grayscale
    # https://www.johndcook.com/blog/2009/08/24/algorithms-convert-color-grayscale/
    @property
    def gray_lightness(self) -> float:
        r, g, b = self.scaled
        return (max(r, g, b) + min(r, g, b)) / 2

    @property
    def gray_average(self) -> float:
        r, g, b = self.scaled
        return statistics.mean([r, g, b])

    @property
    def gray_luminosity(self) -> float:
        r, g, b = self.scaled
        return 0.21 * r + 0.72 * g + 0.07 * b

    # default to luminosity
    grayscale = gray_luminosity

    # other rtn.NpFourTuple subclasses play loosey-goosey with their
    # instance method return types because they're supposed to
    # implement true mathematical operations from linear algebra.
    # Color is different because the its component values
    # have definite meanings, and that's more important than
    # being able to double as a more generic data structure.

    def __add__(self, other: Color) -> Color:
        if not isinstance(other, Color):
            raise NotImplemented
        r, g, b, *_ = self.arr + other.arr
        return Color(r, g, b)

    def __sub__(self, other: Color) -> rtn.NpFourTuple:
        if not isinstance(other, Color):
            raise NotImplemented
        r, g, b, *_ = self.arr - other.arr
        return Color(r, g, b)

    def __neg__(self) -> rtn.NpFourTuple:
        r, g, b, *_ = -self.arr
        return Color(r, g, b)

    def __mul__(self, other) -> Color:
        # hadamard product for colors, otherwise...whatever
        if isinstance(other, Color):
            r = self.r * other.r
            g = self.g * other.g
            b = self.b * other.b
            return Color(r, g, b)
        else:
            r, g, b, *_ = self.arr * other
            return Color(r, g, b)

    def __repr__(self) -> str:
        return f"rtv.Color({list(util.strip_0(n) for n in (self.r, self.g, self.b))})"


class _Colors:
    @property
    def black(self):
        return Color(0, 0, 0)

    @property
    def white(self):
        return Color(1, 1, 1)

    @property
    def grey(self):
        return Color(0.5, 0.5, 0.5)

    @property
    def gray(self):
        return self.grey

    @property
    def red(self):
        return Color(1, 0, 0)

    @property
    def green(self):
        return Color(0, 1, 0)

    @property
    def blue(self):
        return Color(0, 0, 1)


"""
  pre-defined colors made available to the programmer
  as a (writeable, unfortunately) singleton because
  python doesn't easily support static class properties
  (or i would have done this within Color).
"""
Colors = _Colors()


class Canvas:
    """
      a class for manipulating 2D arrays of color values,
      or in other words, a pixel buffer. not sure what
      the difference is in this context between a canvas
      and a pixel buffer, i considered naming this class
      PixelBuffer, but i'm sticking closer to the book
      for now.
    """
    # pixels is an array of Float64 with 3 dimensions, the first two of which
    # are the (unknown) height and width, and the last of which has length 4,
    # corresponding to the size of an Color (which is the size of an rtn.NpFourTuple).
    pixels: NDArray[(typing.Any, typing.Any, 4), Float64]
    _h: int
    _w: int
    name: str
    ppm_line_len = 70

    def __init__(self, width: int, height: int, name: typing.Optional[str] = None) -> None:
        # keep in mind: a canvas is described as width x height,
        # and a matrix is described as rows x columns, but the
        # visual width of a matrix is *actually the number of columns*,
        # while the height is the number of rows. this is going to be
        # important for (x,y) coordinate referencing later as well.
        self._h = height
        self._w = width
        self.pixels = np.full((height, width, 4), Colors.black)

        # name generation based on this blog post:
        # https://www.askpython.com/python/examples/generate-random-strings-in-python
        # important for saving to files later.
        # note that this is only a *somewhat* unique name, not a
        # *guaranteed* unique name, so the occasional conflict is
        # possible (if highly unlikely).
        if not name:
            self.name = util.random_ascii_letter_string(20) + ".ppm"
        # this case (or a better-defined elif branch) might handle
        # the case of loading a PPM file, in which case the name
        # would be set to the name of the file.
        else:
            self.name = name

    @property
    def h(self) -> int:
        return self._h

    @property
    def w(self) -> int:
        return self._w

    height = h
    width = w

    # helper class to make sure Color is used
    # where needed, instead of straight numpy arrays.
    # also...this is the most pythonic way i know of
    # to implement double brackets on a custom class.
    class Row:
        can: Canvas
        row: int

        def __init__(self, canvas: Canvas, rownum: int) -> None:
            self.can = canvas
            self.row = rownum

        def __getitem__(self, key: int) -> Color:
            r, g, b, *_ = self.can.pixels[self.row][key]
            return Color(r, g, b)

        def __setitem__(self, key: int, value: Color) -> None:
            self.can.pixels[self.row][key] = value

        def __len__(self) -> int:
            return len(self.can.pixels[self.row])

        def __iter__(self) -> typing.Iterator[Color]:
            yield from [
                Color(r, g, b) for r, g, b, *_ in self.can.pixels[self.row]
            ]

        def __str__(self) -> str:
            return str(self.can.pixels[self.row])

        def __repr__(self) -> str:
            arr_s = "     " + repr(self.can.pixels[self.row]).lstrip("array")
            return f"rtv.Canvas.Row(\n{arr_s})"

    def __getitem__(self, key: int) -> Canvas.Row:
        return Canvas.Row(self, key)

    # would be nice if i could have type hints say that the length
    # of the second argument must be the same as the width of the
    # canvas but oh well.
    def __setitem__(self, key: int, value: NDArray[(typing.Any, 4), Float64]) -> None:
        if len(value) != self.w:
            raise ValueError(
                f"new row has wrong length ({len(value)}) for canvas, must be ({self.w})")
        self.pixels[key] = value

    def __iter__(self) -> typing.Iterator[Canvas.Row]:
        yield from [Canvas.Row(self, r) for r in range(self.h)]

    @property
    def grayscale(self) -> NDArray[(typing.Any, typing.Any), Float64]:
        """
            all pixels as grayscale
        """
        return np.array([[pix.grayscale for pix in row] for row in self])

    @property
    def grayscale_cv2(self) -> NDArray[(typing.Any, typing.Any), Float64]:
        """
            because if you want to give cv2 a matrix of grayscale
            colors, it expects them to be in range [0,1]
        """
        return np.array([[pix.grayscale / 255 for pix in row] for row in self])

    @property
    def cv2_formatted(self) -> NDArray[(typing.Any, typing.Any), Float64]:
        """
            this isn't how you'd normally display a canvas with cv2,
            but it's helpful if you just need a snapshot of a small-ish canvas
        """
        return np.array([[pix.bgr for pix in row] for row in self])

    def export_to_ppm(self, fname: typing.Optional[str] = None) -> None:
        if not fname:
            fname = self.name

        with open(fname, 'w') as f:
            header = f"P3\n{self.width} {self.height}\n255\n"
            f.write(header)
            for row in self:
                line_len = 0
                for i, pix in enumerate(row, 1):
                    pixels = pix.scaled
                    for j, val in enumerate(pixels):
                        chars_to_write = list(str(
                            val) + ("" if i == len(row) and j == 2 else " "))
                        line_len += len(chars_to_write)
                        if (line_len == self.ppm_line_len) or (j < 2 and line_len + len(str(pixels[j+1])) > self.ppm_line_len - 1):
                            chars_to_write[-1] = '\n'
                            line_len = 0
                        elif line_len > self.ppm_line_len:
                            f.write('\n')
                            line_len = len(chars_to_write)
                        f.write(''.join(chars_to_write))
                        if line_len >= self.ppm_line_len:
                            line_len = 0
                f.write('\n')

    @property
    def img(self) -> Image:
        img = Image.new("RGB", (self.w, self.h))
        for y, row in enumerate(self):
            for x, pix in enumerate(row):
                img.putpixel((x, y), pix.scaled)
        return img

    def show(self) -> None:
        """ 
          a shortcut to PIL.Image.show, only because
          there are shortcuts to the other display methods
        """
        self.img.show()

    # these require %matplotlib inline for jupyter and
    # jupyter qtconsole, or %pylab for straight ipython
    def mp_matshow(self) -> mpi.AxesImage:
        """
          displays using matplotlib.image.matshow on raw canvas pixels
        """
        return plt.matshow(self.pixels)

    def mp_mat_imshow(self) -> mpi.AxesImage:
        """
          displays using matplotlib.image.imshow on raw canvas pixels
        """
        return plt.imshow(self.pixels)

    def mp_img_imshow(self) -> mpi.AxesImage:
        """
          displays using matplotlib.image.imshow on self.img
        """
        return plt.imshow(self.img)

    def __str__(self) -> str:
        return str(self.pixels)

    def __repr__(self) -> str:
        arr_s = "     " + repr(self.pixels).lstrip("array")
        return f"rtv.Canvas(\n{arr_s})"

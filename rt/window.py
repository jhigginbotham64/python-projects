from __future__ import annotations

from abc import ABCMeta, abstractmethod

import cv2
import rt.numeric as rtn
import rt.temporal as rtt
import rt.visible as rtv

import util

"""
  actually drawing things on the actual screen.
  platform-specific code goes in here.
"""


class Window(rtt.TemporalEntity, metaclass=ABCMeta):
    canvas: rtv.Canvas
    title: str

    def __init__(self, canvas: rtv.Canvas, title: str = util.random_ascii_letter_string(20)) -> None:
        super().__init__()
        self.canvas = canvas
        self.title = title

    def __del__(self) -> None:
        super().__del__()

    def update(self, delta: float) -> None:
        if self.is_open:
            self.draw()

    def open(self) -> None:
        self.draw()

    @property
    @abstractmethod
    def is_open(self) -> bool:
        raise NotImplementedError(
            "Window subclasses must implement the is_open property")

    @abstractmethod
    def draw(self) -> None:
        raise NotImplementedError(
            "Window subclasses must implement the draw method")

    @abstractmethod
    def close(self) -> None:
        raise NotImplementedError(
            "Window subclasses must implement the close method")

    # an alternative style is available through the contextlib.contextmanager
    # decorator, which may be useful if the context needs specific arguments:
    # https://stackabuse.com/python-context-managers/
    def __enter__(self) -> Window:
        self.open()
        return self

    def __exit__(self, *exc) -> None:
        self.close()


class OpenCVWindow(Window):
    def __init__(self, canvas: rtv.Canvas, title: str = util.random_ascii_letter_string(20)) -> None:
        super().__init__(canvas, title)

    def __del__(self) -> None:
        super().__del__()

    @property
    def is_open(self) -> bool:
        # https://stackoverflow.com/questions/35003476/opencv-python-how-to-detect-if-a-window-is-closed#37881722
        return cv2.getWindowProperty(self.title, 0) >= 0

    def draw(self) -> None:
        # https://stackoverflow.com/a/51517079
        cv2.imshow(self.title, self.canvas.pixels)
        cv2.waitKey(1)

    def close(self) -> None:
        # https://stackoverflow.com/a/58445169
        if self.is_open:
            cv2.destroyWindow(self.title)


class OpenGLWindow(Window):
    def __init__(self, canvas: rtv.Canvas, title: str = util.random_ascii_letter_string(20)) -> None:
        super().__init__(canvas, title)

    def __del__(self) -> None:
        super().__del__()

    @property
    def is_open(self) -> bool:
        raise NotImplementedError("this method has not been implemented yet")

    def draw(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")

    def close(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")


class DirectXWindow(Window):
    def __init__(self, canvas: rtv.Canvas, title: str = util.random_ascii_letter_string(20)) -> None:
        super().__init__(canvas, title)

    def __del__(self) -> None:
        super().__del__()

    @property
    def is_open(self) -> bool:
        raise NotImplementedError("this method has not been implemented yet")

    def draw(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")

    def close(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")


class VulkanWindow(Window):
    def __init__(self, canvas: rtv.Canvas, title: str = util.random_ascii_letter_string(20)) -> None:
        super().__init__(canvas, title)

    def __del__(self) -> None:
        super().__del__()

    @property
    def is_open(self) -> bool:
        raise NotImplementedError("this method has not been implemented yet")

    def draw(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")

    def close(self) -> None:
        raise NotImplementedError("this method has not been implemented yet")

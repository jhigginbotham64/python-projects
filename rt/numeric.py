from __future__ import annotations
import typing
import math
import numpy as np
from nptyping import NDArray, Float64

import util


# i may decide at some later point that this value
# is too small, or else that enforcing such precision
# offers no practical value to this project
meps = np.finfo(float).eps  # machine epsilon


def eq(a: Float64, b: Float64) -> bool:
    # handle 0 before potentially dividing by it,
    # also cast to float to avoid recursion errors
    if float(b) == 0:
        return float(a) == 0
    # relative rounding error, where a
    # is assumed to be an approximation of b:
    # https://en.wikipedia.org/wiki/Approximation_error
    return abs(1 - a / b) <= meps


def ne(a: Float64, b: Float64) -> bool:
    return not eq(a, b)


def le(a: Float64, b: Float64) -> bool:
    return eq(a, b) or float(a) < float(b)


def ge(a: Float64, b: Float64) -> bool:
    return eq(a, b) or float(a) > float(b)


def lt(a: Float64, b: Float64) -> bool:
    return ne(a, b) and float(a) < float(b)


def gt(a: Float64, b: Float64) -> bool:
    return ne(a, b) and float(a) > float(b)


class MepsFloat(np.float64):
    """
        used to enforce meps comparisons when needed.
        numpy coerces array entries to its own structured
        types, so i can't enforce meps comparison across
        the board or set it as the default, but it's handy
        to be able to break out in key places.

        i'm pondering the possibility of flat-out truncating
        numbers that are equal to themselves if rounded to
        15 or so places. not sure that's a good idea but it's
        definitely doable.
    """
    def __new__(cls, value):
        return super(MepsFloat, cls).__new__(cls, np.float64(value))

    def __eq__(self, other):
        return eq(self, other)

    def __ne__(self, other):
        return ne(self, other)

    def __le__(self, other):
        return le(self, other)

    def __ge__(self, other):
        return ge(self, other)

    def __lt__(self, other):
        return lt(self, other)

    def __gt__(self, other):
        return gt(self, other)

    def __round__(self, ndigits: typing.Optional[int] = None) -> MepsFloat:
        return MepsFloat(round(float(self), ndigits))


class NpFourTuple:
    # numpy-based 4-tuple (x, y, z, w)
    arr: NDArray[(4,), Float64]

    def __init__(self, x: Float64, y: Float64, z: Float64, w: Float64) -> None:
        self.arr = np.array([x, y, z, w])

    def __getitem__(self, key: int) -> Float64:
        return self.arr[key]

    def __setitem__(self, key: int, value: Float64) -> None:
        self.arr[key] = value

    def __contains__(self, key: Float64) -> bool:
        return key in self.arr

    def __iter__(self) -> typing.Iterator[MepsFloat]:
        yield from [MepsFloat(n) for n in self.arr]

    def __len__(self) -> int:
        return len(self.arr)

    def get_x(self) -> MepsFloat:
        return MepsFloat(self[0])

    def set_x(self, value: Float64) -> None:
        self[0] = value

    def get_y(self) -> MepsFloat:
        return MepsFloat(self[1])

    def set_y(self, value: Float64) -> None:
        self[1] = value

    def get_z(self) -> MepsFloat:
        return MepsFloat(self[2])

    def set_z(self, value: Float64) -> None:
        self[2] = value

    def get_w(self) -> MepsFloat:
        return MepsFloat(self[3])

    def set_w(self, value: Float64) -> None:
        self[3] = value

    x = property(get_x, set_x)
    y = property(get_y, set_y)
    z = property(get_z, set_z)
    w = property(get_w, set_w)

    @property
    def is_vector(self) -> bool:
        return self.w == 0

    @property
    def is_point(self) -> bool:
        return self.w == 1

    @property
    def as_vector(self) -> typing.Optional[Vector]:
        if not self.is_vector:
            return None
        x, y, z, *_ = self
        return Vector(x, y, z)

    @property
    def as_point(self) -> typing.Optional[Point]:
        if not self.is_point:
            return None
        x, y, z, *_ = self
        return Point(x, y, z)

    def __eq__(self, other: NpFourTuple) -> bool:
        return len(self) == len(other) and all([eq(self[i], other[i]) for i in range(len(self))])

    def __add__(self, other: NpFourTuple) -> NpFourTuple:
        if not isinstance(other, NpFourTuple):
            raise NotImplemented
        x, y, z, w = self.arr + other.arr
        return NpFourTuple(x, y, z, w)

    def __sub__(self, other: NpFourTuple) -> NpFourTuple:
        if not isinstance(other, NpFourTuple):
            raise NotImplemented
        x, y, z, w = self.arr - other.arr
        return NpFourTuple(x, y, z, w)

    def __neg__(self) -> NpFourTuple:
        x, y, z, w = -self.arr
        return NpFourTuple(x, y, z, w)

    def __mul__(self, other) -> NpFourTuple:
        x, y, z, w = self.arr * other
        return NpFourTuple(x, y, z, w)

    def __matmul__(self, other) -> NpFourTuple:
        # insist on arr, i'll improve error checking later if i need to
        x, y, z, w = self.arr @ other.arr
        res = NpFourTuple(x, y, z, w)
        if res.is_vector:
            return res.as_vector
        elif res.is_point:
            return res.as_point
        return res

    def __truediv__(self, other) -> NpFourTuple:
        x, y, z, w = self.arr / other
        return NpFourTuple(x, y, z, w)

    def __floordiv__(self, other) -> NpFourTuple:
        x, y, z, w = self.arr // other
        return NpFourTuple(x, y, z, w)

    def __mod__(self, other) -> NpFourTuple:
        x, y, z, w = self.arr % other
        return NpFourTuple(x, y, z, w)

    def __pow__(self, other) -> NpFourTuple:
        x, y, z, w = self.arr ** other
        return NpFourTuple(x, y, z, w)

    def __round__(self, ndigits: typing.Optional[int] = None) -> NpFourTuple:
        return NpFourTuple(round(self.x, ndigits), round(self.y, ndigits), round(self.z, ndigits), round(self.w, ndigits))

    def __str__(self) -> str:
        return str(self.arr)

    def __repr__(self) -> str:
        return f"rtn.NpFourTuple({list(util.strip_0(n) for n in self)})"


class Vector(NpFourTuple):
    def __init__(self, x: Float64, y: Float64, z: Float64) -> None:
        super().__init__(x, y, z, 0.0)

    @property
    def norm(self) -> MepsFloat:
        return MepsFloat(np.linalg.norm(self.arr))

    @property
    def normalized(self) -> Vector:
        x, y, z, *_ = self / self.norm
        return Vector(x, y, z)

    def dot(self, other: Vector) -> MepsFloat:
        if not isinstance(other, NpFourTuple):
            raise NotImplemented
        return MepsFloat(np.dot(self.arr, other.arr))

    def cross(self, other: Vector) -> Vector:
        if not isinstance(other, Vector):
            raise NotImplemented
        return Vector(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x
        )

    def scale(self, s: Vector) -> Vector:
        tr = scaling(s.x, s.y, s.z) @ self
        return Vector(tr.x, tr.y, tr.z)

    def reflect_x(self) -> Vector:
        tr = x_reflection() @ self
        return Vector(tr.x, tr.y, tr.z)

    def reflect_y(self) -> Vector:
        tr = y_reflection() @ self
        return Vector(tr.x, tr.y, tr.z)

    def reflect_z(self) -> Vector:
        tr = z_reflection() @ self
        return Vector(tr.x, tr.y, tr.z)

    def rotate_x(self, rad: float) -> Vector:
        tr = x_rotation(rad) @ self
        return Vector(tr.x, tr.y, tr.z)

    def rotate_y(self, rad: float) -> Vector:
        tr = y_rotation(rad) @ self
        return Vector(tr.x, tr.y, tr.z)

    def rotate_z(self, rad: float) -> Vector:
        tr = z_rotation(rad) @ self
        return Vector(tr.x, tr.y, tr.z)

    def shear(self, xy: float, xz: float, yx: float, yz: float, zx: float, zy: float) -> Vector:
        tr = shearing(xy, xz, yx, yz, zx, zy) @ self
        return Vector(tr.x, tr.y, tr.z)

    # the NpFourTuples returned by the parent's version
    # of these function would all be Vectors anyhow
    # because these operations result in w = 0 for
    # Vectors, so i override them here to return Vectors.
    def __neg__(self) -> Vector:
        x, y, z, *_ = -self.arr
        return Vector(x, y, z)

    def __mul__(self, other) -> Vector:
        x, y, z, *_ = self.arr * other
        return Vector(x, y, z)

    def __truediv__(self, other) -> Vector:
        x, y, z, *_ = self.arr / other
        return Vector(x, y, z)

    def __floordiv__(self, other) -> Vector:
        x, y, z, *_ = self.arr // other
        return Vector(x, y, z)

    def __mod__(self, other) -> Vector:
        x, y, z, *_ = self.arr % other
        return Vector(x, y, z)

    def __pow__(self, other) -> Vector:
        x, y, z, *_ = self.arr ** other
        return Vector(x, y, z)

    def __round__(self, ndigits: typing.Optional[int] = None) -> Vector:
        return Vector(round(self.x, ndigits), round(self.y, ndigits), round(self.z, ndigits))

    def __repr__(self) -> str:
        return f"rtn.Vector({list(util.strip_0(n) for n in (self.x, self.y, self.z))})"


class Point(NpFourTuple):
    def __init__(self, x: Float64, y: Float64, z: Float64) -> None:
        super().__init__(x, y, z, 1.0)

    def translate(self, t: Vector) -> Point:
        tr = translation(t.x, t.y, t.z) @ self
        return Point(tr.x, tr.y, tr.z)

    def scale(self, s: Vector) -> Point:
        tr = scaling(s.x, s.y, s.z) @ self
        return Point(tr.x, tr.y, tr.z)

    def reflect_x(self) -> Point:
        tr = x_reflection() @ self
        return Point(tr.x, tr.y, tr.z)

    def reflect_y(self) -> Point:
        tr = y_reflection() @ self
        return Point(tr.x, tr.y, tr.z)

    def reflect_z(self) -> Point:
        tr = z_reflection() @ self
        return Point(tr.x, tr.y, tr.z)

    def rotate_x(self, rad: float) -> Point:
        tr = x_rotation(rad) @ self
        return Point(tr.x, tr.y, tr.z)

    def rotate_y(self, rad: float) -> Point:
        tr = y_rotation(rad) @ self
        return Point(tr.x, tr.y, tr.z)

    def rotate_z(self, rad: float) -> Point:
        tr = z_rotation(rad) @ self
        return Point(tr.x, tr.y, tr.z)

    def shear(self, xy: float, xz: float, yx: float, yz: float, zx: float, zy: float) -> Point:
        tr = shearing(xy, xz, yx, yz, zx, zy) @ self
        return Point(tr.x, tr.y, tr.z)

    def __round__(self, ndigits: typing.Optional[int] = None) -> Point:
        return Point(round(self.x, ndigits), round(self.y, ndigits), round(self.z, ndigits))

    def __repr__(self) -> str:
        return f"rtn.Point({list(util.strip_0(n) for n in (self.x, self.y, self.z))})"


class Matrix:
    arr: NDArray[(typing.Any, typing.Any), Float64]

    def __init__(self, arr) -> None:
        # punt to numpy for input validation
        self.arr = np.array(arr, dtype=np.float64)
        if self.arr.ndim == 1:
            self.arr = np.array([self.arr])
        elif self.arr.ndim > 2:
            raise ValueError(
                "only 2D matrices of 64-bit floats are supported at this time")

    @property
    def shape(self) -> typing.Tuple[int, int]:
        return self.arr.shape

    # m and n, as in, an m x n matrix
    # has m rows and n columns
    @property
    def m(self) -> int:
        # rows
        return self.shape[0]

    @property
    def n(self) -> int:
        # columns
        return self.shape[1]

    nrows = m
    ncolumns = n

    # i actually re-used the Row pattern thing
    # from Canvas, which was written first
    class Row:
        mat: Matrix
        row: int

        def __init__(self, matrix: Matrix, rownum: int) -> None:
            self.mat = matrix
            self.row = rownum

        def __getitem__(self, key: int) -> MepsFloat:
            return MepsFloat(
                self.mat.arr[self.row][key]
            )

        def __setitem__(self, key: int, value: Float64) -> None:
            self.mat.arr[self.row][key] = value

        def __len__(self) -> int:
            return len(self.mat.arr[self.row])

        def __iter__(self) -> typing.Iterator[MepsFloat]:
            yield from [
                MepsFloat(a) for a in self.mat.arr[self.row]
            ]

        def __str__(self) -> str:
            return str(self.mat.arr[self.row])

        def __repr__(self) -> str:
            arr_s = "     " + repr(self.mat.arr[self.row]).lstrip("array")
            return f"rtn.Matrix.Row(\n{arr_s})"

    def __getitem__(self, key: int) -> Matrix.Row:
        return Matrix.Row(self, key)

    def __setitem__(self, key: int, value: NDArray[(typing.Any,), Float64]) -> None:
        if len(value) != self.n:
            raise ValueError(
                f"new row has wrong length ({len(value)}) for matrix, must be ({self.n})")
        self.arr[key] = value

    def __iter__(self) -> typing.Iterator[Matrix.Row]:
        yield from [
            Matrix.Row(self, r) for r in range(self.m)
        ]

    @property
    def is_4tuple(self) -> bool:
        return self.shape == (1, 4)

    @property
    def is_vector(self) -> bool:
        return self.is_4tuple and self[0][3] == 0

    @property
    def is_point(self) -> bool:
        return self.is_4tuple and self[0][3] == 1

    @property
    def as_4tuple(self) -> typing.Optional[NpFourTuple]:
        if not self.is_4tuple:
            return None
        x, y, z, w = self[0]
        # assume that adding subclass functionality
        # to returned values is the polite thing to
        # do where possible
        if w == 0:
            return Vector(x, y, z)
        elif w == 1:
            return Point(x, y, z)
        return NpFourTuple(x, y, z, w)

    @property
    def as_vector(self) -> typing.Optional[Vector]:
        if not self.is_vector:
            return None
        x, y, z, *_ = self[0]
        return Vector(x, y, z)

    @property
    def as_point(self) -> typing.Optional[Point]:
        if not self.is_point:
            return None
        x, y, z, *_ = self[0]
        return Point(x, y, z)

    def check_valid_for_boolean_cmp(self, other) -> bool:
        if isinstance(other, NpFourTuple):
            if not self.is_4tuple:
                raise ValueError(
                    "matrices must have same dimensions to use logical comparisons")
        elif not isinstance(other, Matrix):
            raise NotImplemented
        elif self.shape != other.shape:
            raise ValueError(
                "matrices must have same dimensions to use logical comparisons")
        return True

    def __eq__(self, other: Matrix) -> bool:
        if self.check_valid_for_boolean_cmp(other):
            # enforcing meps for this would sacrifice the
            # elegance of numpy, so i'll pass until a use
            # case arises
            return (self.arr == other.arr).all()

    def __matmul__(self, other) -> typing.Union[Matrix, Vector, Point, NpFourTuple]:
        # if people want to multiply with other valid matrices,
        # they can bypass this function and use arr directly or
        # something, but the "official" one needs to preserve
        # proper return types.
        if not isinstance(other, Matrix) and not isinstance(other, NpFourTuple):
            raise NotImplementedError(
                f"matrix multiplication not implemented for {str(type(other))}")
        res = Matrix(self.arr @ other.arr)
        if res.is_vector:
            return res.as_vector
        elif res.is_point:
            return res.as_point
        elif res.is_4tuple:
            return res.as_4tuple
        return res

    @property
    def transpose(self) -> Matrix:
        return Matrix(self.arr.transpose())

    t = transpose

    @staticmethod
    def identity(dim: int = 4) -> Matrix:
        return Matrix(np.identity(dim))

    i = identity

    # eye is actually somethiing separate and a bit
    # more robust that can be implemented if the
    # need arises

    @property
    def determinant(self) -> MepsFloat:
        # numpy's rounding error is a tad larger than i'd like,
        # maybe one of these days i'll do some more research to
        # figure out exactly how big a deal it is. i had been
        # rounding the result to 11 places before returing it
        # until i realized that i could add __round__ to my
        # classes. then i realized that the caller should be the
        # one who decides how much numerical accuracy matters.
        return MepsFloat(np.linalg.det(self.arr))

    det = determinant

    def submatrix(self, row: int, col: int) -> Matrix:
        rows = [r for r in range(self.m) if r != row]
        cols = [c for c in range(self.n) if c != col]
        return Matrix(self.arr[np.ix_(rows, cols)])

    sub = submatrix

    def minor(self, row: int, col: int) -> MepsFloat:
        return self.sub(row, col).det

    def cofactor(self, row: int, col: int) -> MepsFloat:
        return self.minor(row, col) * (-1) ** (row + col)

    @property
    def invertible(self) -> bool:
        return self.det != 0

    @property
    def inverse(self) -> Matrix:
        # let the caller check invertibility, numpy
        # will raise an exception if they don't
        return Matrix(np.linalg.inv(self.arr))

    inv = inverse

    def __round__(self, ndigits: typing.Optional[int] = None) -> Matrix:
        # especially useful for checking against a less-precise oracle
        return Matrix([
            [round(a, ndigits) for a in r]
            for r in self
        ])

    def __str__(self) -> str:
        return str(self.arr)

    def __repr__(self) -> str:
        arr_s = "     " + repr(self.arr).lstrip("array")
        return f"rtn.Matrix(\n{arr_s})"


def translation(x: float, y: float, z: float) -> Matrix:
    return Matrix([
        [1, 0, 0, x],
        [0, 1, 0, y],
        [0, 0, 1, z],
        [0, 0, 0, 1]
    ])


def scaling(x: float, y: float, z: float) -> Matrix:
    return Matrix([
        [x, 0, 0, 0],
        [0, y, 0, 0],
        [0, 0, z, 0],
        [0, 0, 0, 1]
    ])


def x_reflection() -> Matrix:
    return scaling(-1, 1, 1)


def y_reflection() -> Matrix:
    return scaling(1, -1, 1)


def z_reflection() -> Matrix:
    return scaling(1, 1, -1)


def x_rotation(rad: float) -> Matrix:
    return Matrix([
        [1, 0, 0, 0],
        [0, math.cos(rad), -math.sin(rad), 0],
        [0, math.sin(rad), math.cos(rad), 0],
        [0, 0, 0, 1]
    ])


def y_rotation(rad: float) -> Matrix:
    return Matrix([
        [math.cos(rad), 0, math.sin(rad), 0],
        [0, 1, 0, 0],
        [-math.sin(rad), 0, math.cos(rad), 0],
        [0, 0, 0, 1]
    ])


def z_rotation(rad: float) -> Matrix:
    return Matrix([
        [math.cos(rad), -math.sin(rad), 0, 0],
        [math.sin(rad), math.cos(rad), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ])


def shearing(xy: float, xz: float, yx: float, yz: float, zx: float, zy: float) -> Matrix:
    return Matrix([
        [1, xy, xz, 0],
        [yx, 1, yz, 0],
        [zx, zy, 1, 0],
        [0, 0, 0, 1]
    ])

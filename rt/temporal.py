from __future__ import annotations
import time
import typing
from abc import ABCMeta, abstractmethod
from threading import Thread, Event


class TemporalEntity(metaclass=ABCMeta):
    elist = list()  # entity list

    # NOTE to hook into temporal updates, derived classes
    # must define both __init__ and __del__ methods which
    # at least call super().__init__() and super().__del__(),
    # *in addition to* overriding update.
    def __init__(self, *args, **kwargs) -> None:
        # print("PhysicsEntity constructor")
        TemporalEntity.elist.append(self)

    def __del__(self) -> None:
        TemporalEntity.elist.remove(self)

    @staticmethod
    def update_all(delta: float) -> None:
        for e in TemporalEntity.elist:
            e.update(delta)

    @abstractmethod
    def update(self, delta: float):
        # a la UnityEngine.MonoBehavior :)
        raise NotImplementedError(
            "TemporalEntities must implement update")

# the advance_clock function assumes nanosecond input,
# which it converts to seconds, since this is how
# it is used by DeltaProcessClass.run.
# the tick_*s functions on the other hand allow advancing
# the clock by units of time hinted at in their names,
# and handle the conversions themselves.


def advance_clock(delta: float) -> None:
    delta /= 10 ** 9   # convert nanoseconds to seconds
    TemporalEntity.update_all(delta)


# seconds
def tick_s(delta: float = 0.1) -> None:
    delta *= 10 ** 9
    advance_clock(delta)


# milliseconds
def tick_ms(delta: float = 0.1) -> None:
    delta *= 10 ** 6
    advance_clock(delta)


# microseconds
def tick_us(delta: float = 0.1) -> None:
    delta *= 10 ** 3
    advance_clock(delta)


# nanoseconds
def tick_ns(delta: float = 0.1) -> None:
    advance_clock(delta)


tick = tick_s  # default to advance by seconds


class DeltaProcess(Thread):
    """
        basically a helper class that lets you update the clock
        in a thread rather than ticking manually. can be used
        for other delta functions that do other things, but by
        default it runs the clock to invoke update on your
        TemporalEntities.
    """

    def __init__(self, interval: float = 0.01, delta_func: typing.Callable[[float], None] = advance_clock) -> None:
        Thread.__init__(self)
        self.interval = interval
        self.stopped = Event()
        self.create_time = time.time_ns()
        self.delta_func = delta_func

    def run(self):
        # https://stackoverflow.com/questions/12435211/python-threading-timer-repeat-function-every-n-seconds
        self.time = time.time_ns()
        while not self.stopped.wait(self.interval):
            delta = time.time_ns() - self.time
            self.time += delta
            self.delta_func(delta)

    def stop(self):
        self.stopped.set()

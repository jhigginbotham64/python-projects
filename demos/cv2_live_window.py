import numpy as np
import cv2


# reworked from https://stackoverflow.com/a/51517079
# in order to shed more light on how cv2.imshow works
class main:

    z = None

    @staticmethod
    def sin2d(x, y):
        """2-d sine function to plot"""
        return np.sin(x) + np.cos(y)

    @staticmethod
    def getFrame(w, h):
        """Generate next frame of simulation as numpy array"""

        # Create data on first call only
        if main.z is None:
            xx, yy = np.meshgrid(np.linspace(0, 2*np.pi, w),
                                 np.linspace(0, 2*np.pi, h))
            main.z = main.sin2d(xx, yy)
            main.z = cv2.normalize(
                main.z, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

        # Just roll data for subsequent calls
        main.z = np.roll(main.z, (1, 2), (0, 1))
        return main.z

    @staticmethod
    def run(iterations: int = 1000, width: int = 990, height: int = 550):

        for i in range(iterations):

            # Get a numpy array to display from the simulation
            npimage = main.getFrame(width, height)

            cv2.imshow('image', npimage)
            cv2.waitKey(1)

        cv2.destroyAllWindows()

    @staticmethod
    def reset():
        # reset for further testing, but wait to be called
        # manually so that main.z is available for inspection
        # after main.run
        main.z = None


if __name__ == "__main__":
    main.run()

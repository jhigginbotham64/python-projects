import os
import math
import typing
import time
import cv2

import pytest
from conftest import *

import rt.numeric as rtn
import rt.temporal as rtt
import rt.visible as rtv
import rt.window as rtw

import util

nums = [4.3, -4.2, 3.1]
x, y, z = nums

# NOTE T is for "Test", which i can't use directly without
# pytest giving warnings about "can't collect class"


# a little helper function for while i figure out opencv
def show_with_cv2(arr):
    for i in range(1000):
        cv2.imshow("test", arr)
        cv2.waitKey(1)
    cv2.destroyWindow("test")


class TTemporalEntity(rtt.TemporalEntity):
    def __init__(self, name: str) -> None:
        super().__init__()
        self.name = name
        self.ticked = False

    def __del__(self) -> None:
        super().__del__()

    def update(self, delta: float) -> None:
        # print(f"{self.name} update")
        self.ticked = True


class TProjectile(rtt.TemporalEntity):
    canv: rtv.Canvas
    pos: rtn.Point
    vel: rtn.Vector
    grav: rtn.Vector
    wind: rtn.Vector
    col: rtv.Color
    rtdp: rtt.DeltaProcess
    last_delta: rtn.MepsFloat

    def __init__(self, interval: float = 0.01, canvas: typing.Optional[rtv.Canvas] = None, start: rtn.Point = rtn.Point(0, 1, 0), gravity: rtn.Vector = rtn.Vector(0, -10, 0), wind: rtn.Vector = rtn.Vector(-1, 0, 0), color: rtv.Color = rtv.Colors.red, bgr: bool = False) -> None:
        super().__init__()
        # NOTE default values are singular, as in, a default
        # value which is an instance of a class is actually the
        # same instance across multiple calls. which means that
        # if you give the canvas parameter a default value, the
        # same canvas gets used by every TProjectile in the whole
        # test suite run. which is confusing if part of how you
        # test things is drawing the same thing a bunch of different
        # ways (which is the whole point of this class).
        if canvas:
            self.canv = canvas
        else:
            self.canv = rtv.Canvas(200, 100)
        self.pos = start
        # baselined for a 200 x 100 canvas, breaks for sizes
        # too much larger, also assumes 2:1 width to height ratio
        self.vel = rtn.Vector(1, 1.8, 9).normalized * 225 * self.canv.h / 100
        self.grav = gravity
        self.wind = wind
        self.col = color if not bgr else color.bgr
        self.rtdp = rtt.DeltaProcess(interval=interval)
        self.last_delta = 0

        x, y = self.canv_coord
        self.canv[y][x] = self.col

        # self.print_debug()

    def __del__(self) -> None:
        super().__del__()

    @property
    def canv_coord(self) -> typing.Tuple[int, int]:
        # returns (x, y) on canvas based on current position
        return int(round(self.pos.x)), int(round(self.canv.h - self.pos.y))

    @property
    def out_of_bounds(self) -> bool:
        x, y = self.canv_coord
        return x < 0 or y < 0 or x >= self.canv.w or y >= self.canv.h

    @property
    def bad_delta(self) -> bool:
        return util.truncate_float(self.last_delta, 2) > self.rtdp.interval

    def print_debug(self) -> None:
        x, y = self.canv_coord
        print(f"x = {x}, y = {y}, pos = {self.pos}, vel = {self.vel}")

    def update(self, delta: float) -> None:
        # print(f"delta = {delta}")

        # testing the consistency of timers in rt.temporal
        self.last_delta = delta
        if self.bad_delta:
            self.rtdp.stop()
            return

        self.pos = self.pos + self.vel * delta
        self.vel = self.vel + \
            (self.grav + self.wind) * delta
        # self.print_debug()
        if self.out_of_bounds:
            self.rtdp.stop()
        else:
            x, y = self.canv_coord
            self.canv[y][x] = self.col

    def launch(self) -> None:
        self.rtdp.start()
        self.rtdp.join()

# NOTE TProjectile reminds me of of Godot Engine's Sprite,
# which requires something like Node2D, which requires
# groups and transforms and a scene hierarchy and automatic
# drawing. all of which will be within reach before the
# end of the book.


class TestCh1:
    def test_mepsfloat(self):
        a = rtn.MepsFloat(1)
        b = rtn.MepsFloat(2)
        c = a + b

        # first, operators
        assert a == 1
        assert not a < 1
        assert not a > 1
        assert a <= 1
        assert a >= 1

        assert b == 2
        assert not b < 2
        assert not b > 2
        assert b <= 2
        assert b >= 2

        assert c == 3
        assert not c < 3
        assert not c > 3
        assert c <= 3
        assert c >= 3

        assert a != b
        assert a != c
        assert b != c
        assert a < b
        assert b > a
        assert a <= b
        assert b >= a
        assert a < c and b < c
        assert c > a and c > b
        assert a <= c and b <= c
        assert c >= a and c >= b

        # second, rounding error
        a2 = 1.0000000000000001
        assert a == a2
        assert not a != a2
        assert not a < a2
        assert not a > a2
        assert a <= a2
        assert a >= a2

        b2 = 2.00000000000000007
        assert b == b2
        assert not b != b2
        assert not b < b2
        assert not b > b2
        assert b <= b2
        assert b >= b2

        c = rtn.MepsFloat(c)
        c2 = a2 + b2
        assert c == c2
        assert not c != c2
        assert not c < c2
        assert not c > c2
        assert c <= c2
        assert c >= c2

    def test_tuple(self):
        # points
        a = rtn.NpFourTuple(x, y, z, 1.0)
        assert a.x == x
        assert a.y == y
        assert a.z == z
        assert a.w == 1.0
        assert a == rtn.Point(x, y, z)
        assert a != rtn.Vector(x, y, z)

        # vectors
        a = rtn.NpFourTuple(x, y, z, 0.0)
        assert a.x == x
        assert a.y == y
        assert a.z == z
        assert a.w == 0.0
        assert a != rtn.Point(x, y, z)
        assert a == rtn.Vector(x, y, z)

    def test_point(self):
        assert rtn.Point(x, y, z) == rtn.NpFourTuple(x, y, z, 1.0)

    def test_vector(self):
        assert rtn.Vector(x, y, z) == rtn.NpFourTuple(x, y, z, 0.0)

    def test_tuple_add(self):
        a1 = rtn.NpFourTuple(3, -2, 5, 1)
        a2 = rtn.NpFourTuple(-2, 3, 1, 0)
        assert a1 + a2 == rtn.NpFourTuple(1, 1, 6, 1)
        assert a2 + a1 == rtn.NpFourTuple(1, 1, 6, 1)

    def test_tuple_sub(self):
        # point - point
        p1 = rtn.Point(3, 2, 1)
        p2 = rtn.Point(5, 6, 7)
        assert p1 - p2 == rtn.Vector(-2, -4, -6)
        assert p2 - p1 == rtn.Vector(2, 4, 6)

        # point - vector, vector - point
        p = rtn.Point(3, 2, 1)
        v = rtn.Vector(5, 6, 7)
        assert p - v == rtn.Point(-2, -4, -6)
        assert v - p == rtn.NpFourTuple(2, 4, 6, -1)

        # vector - vector
        v1 = rtn.Vector(3, 2, 1)
        v2 = rtn.Vector(5, 6, 7)
        assert v1 - v2 == rtn.Vector(-2, -4, -6)
        assert v2 - v1 == rtn.Vector(2, 4, 6)

    def test_tuple_neg(self):
        assert rtn.Vector(0, 0, 0) - rtn.Vector(1, -2,
                                                3) == rtn.Vector(-1, 2, -3)
        assert -rtn.NpFourTuple(1, -2, 3, -4) == rtn.NpFourTuple(-1, 2, -3, 4)

    def test_tuple_scalar_mult(self):
        assert rtn.NpFourTuple(1, -2, 3, -4) * \
            3.5 == rtn.NpFourTuple(3.5, -7, 10.5, -14)
        assert rtn.NpFourTuple(1, -2, 3, -4) * \
            0.5 == rtn.NpFourTuple(0.5, -1, 1.5, -2)

    def test_tuple_scalar_div(self):
        assert rtn.NpFourTuple(1, -2, 3, -4) / \
            2 == rtn.NpFourTuple(0.5, -1, 1.5, -2)

    def test_vector_magnitude(self):
        assert rtn.Vector(1, 0, 0).norm == 1
        assert rtn.Vector(0, 1, 0).norm == 1
        assert rtn.Vector(0, 0, 1).norm == 1
        assert rtn.Vector(1, 2, 3).norm == math.sqrt(14)
        assert rtn.Vector(-1, -2, -3).norm == math.sqrt(14)

    def test_vector_norm(self):
        assert rtn.Vector(4, 0, 0).normalized == rtn.Vector(1, 0, 0)
        assert rtn.Vector(1, 2, 3).normalized == rtn.Vector(
            1/math.sqrt(14), 2/math.sqrt(14), 3/math.sqrt(14))
        assert rtn.Vector(1, 2, 3).normalized.norm == 1

    def test_tuple_dot(self):
        assert rtn.Vector(1, 2, 3).dot(rtn.Vector(2, 3, 4)) == 20

    def test_vector_cross(self):
        a = rtn.Vector(1, 2, 3)
        b = rtn.Vector(2, 3, 4)
        assert a.cross(b) == rtn.Vector(-1, 2, -1)
        assert b.cross(a) == rtn.Vector(1, -2, 1)

    def test_temporal_manual_tick(self):
        t1 = TTemporalEntity("t1")
        t2 = TTemporalEntity("t2")

        assert not t1.ticked
        assert not t2.ticked

        rtt.tick()

        assert t1.ticked
        assert t2.ticked

    def test_temporal_threaded(self):
        t1 = TTemporalEntity("t1")
        t2 = TTemporalEntity("t2")

        assert not t1.ticked
        assert not t2.ticked

        temporality = rtt.DeltaProcess()

        temporality.start()

        time.sleep(0.1)

        temporality.stop()

        assert t1.ticked
        assert t2.ticked


class TestCh2:
    def test_color(self):
        c = rtv.Color(-0.5, 0.4, 1.7)
        assert c.red == -0.5
        assert c.green == 0.4
        assert c.blue == 1.7

    def test_color_add(self):
        c1 = rtv.Color(0.9, 0.6, 0.75)
        c2 = rtv.Color(0.7, 0.1, 0.25)
        c3 = c1 + c2
        assert isinstance(c3, rtv.Color)
        assert c3 == rtv.Color(1.6, 0.7, 1.0)

    def test_color_sub(self):
        c1 = rtv.Color(0.9, 0.6, 0.75)
        c2 = rtv.Color(0.7, 0.1, 0.25)
        c3 = c1 - c2
        assert isinstance(c3, rtv.Color)
        assert c3 == rtv.Color(0.2, 0.5, 0.5)

    def test_color_mult(self):
        # scalar mult
        c = rtv.Color(0.2, 0.3, 0.4)
        cx2 = c * 2
        assert isinstance(cx2, rtv.Color)
        assert cx2 == rtv.Color(0.4, 0.6, 0.8)

        # hadamard product
        c1 = rtv.Color(1, 0.2, 0.4)
        c2 = rtv.Color(0.9, 1, 0.1)
        c3 = c1 * c2
        assert isinstance(c3, rtv.Color)
        assert c3 == rtv.Color(0.9, 0.2, 0.04)

    def test_canvas(self):
        c = rtv.Canvas(10, 20)
        assert c.width == 10
        assert c.height == 20
        for r in c:
            for pixel in r:
                assert isinstance(pixel, rtv.Color)
                assert pixel == rtv.Colors.black

    def test_canvas_pixel_write(self):
        c = rtv.Canvas(10, 20)
        c[2][3] = rtv.Colors.red
        assert isinstance(c[2][3], rtv.Color)
        assert c[2][3] == rtv.Colors.red

    """
        i could have used fixtures for the ppm tests,
        but for now i like that it leaves the files
        around for inspection if the tests fail.
        the danger of them getting added to source
        control is small and remote. :P
    """

    def test_ppm_header(self):
        c = rtv.Canvas(5, 3)
        fname = "./test_ppm_header.ppm"
        c.export_to_ppm(fname)
        with open(fname) as ppm:
            lines = [line.strip() for line in ppm]
            assert lines[0] == "P3"
            assert lines[1] == "5 3"
            assert lines[2] == "255"
        os.remove(fname)

    def test_ppm_pixel_data(self):
        c = rtv.Canvas(5, 3)
        fname = "./test_ppm_pixel_data.ppm"
        # while writing this test, i again ran into the fact
        # that matrix and screen reference coordinates are swapped:
        # the book's write_pixel function uses (x,y), which translates
        # to c[y][x] = whatever.
        c[0][0] = rtv.Color(1.5, 0, 0)
        c[1][2] = rtv.Color(0, 0.5, 0)
        c[2][4] = rtv.Color(-0.5, 0, 1)
        c.export_to_ppm(fname)
        with open(fname) as ppm:
            lines = [line.strip() for line in ppm]
            assert lines[3] == "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
            assert lines[4] == "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0"
            assert lines[5] == "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"
        os.remove(fname)

    def test_ppm_line_length(self):
        c = rtv.Canvas(10, 2)
        for i, r in enumerate(c):
            for j, pixel in enumerate(r):
                c[i][j] = rtv.Color(1, 0.8, 0.6)
        fname = "./test_ppm_line_length.ppm"
        c.export_to_ppm(fname)
        with open(fname) as ppm:
            lines = [line for line in ppm]
            print(f"number of lines: {len(lines)}")
            assert all(len(line) <= c.ppm_line_len for line in lines)
            assert len(lines) == 7
            assert lines[3] == "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
            assert lines[4] == "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
            assert lines[5] == "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
            assert lines[6] == "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
            # the book has an additional test to make sure the file is newline-terminated,
            # but that is accomplished by the check on the number of lines and the last line test
        os.remove(fname)

    def test_projectile_anim_threaded(self):
        p = TProjectile(interval=0.01)
        p.launch()
        # the formulation of this next condition is more than what is strictly necessary
        # for testing, but it plays nicely with pytest's magnificent variable introspection
        # and thus is a bit more useful than simply "assert not p.bad_delta".
        assert p.last_delta == 0.01 or not p.bad_delta

    def test_projectile_anim_live_opencv(self):
        """
            the main pass criteria for this test is that the familiar
            projectile curve gets drawn live on a new window, which
            opens automatically and then closes. this must be verified
            visually. pytest only cares that no errors are thrown.

            the behind-the-scenes of these three lines is a bit magical.
            recall that TProjectile creates a DeltaProcess, and its launch
            function starts this thread and then joins it. TProjectile
            and rtw.Window are both TemporalEntities, and TProjectile gets
            created first, so what happens is the following:

            1) TProjectile gets created and colors the lower-left canvas pixel
            2) a new Window is created and displayed using the projectile's
                canvas, so the initial canvas state (lower-left pixel colored)
                is apparent in the window
            3) the projectile launches and starts its time thread
            4) the projectile was created first, so its update function executes
                first and colors (or doesn't color) the next canvas pixel
            5) the window was created second, so its update function draws the
                just-updated projectile canvas
            6) this continues until the projectile "lands", resulting in a live
                display of the projectile "arc" as it travels
            7) the time thread is joined and launch returns, exiting the with
                block
            8) Window's context manager closes the window

            pretty nifty, huh?
        """
        p = TProjectile(bgr=True)  # bgr because opencv
        with rtw.OpenCVWindow(p.canv) as w:
            p.launch()

    @pytest.mark.skip("OpenGLWindow has not been implemented")
    def test_projectile_anim_live_opengl(self):
        p = TProjectile()
        with rtw.OpenGLWindow(p.canv) as w:
            p.launch()

    @pytest.mark.skip("DirectXWindow has not been implemented")
    def test_projectile_anim_live_directx(self):
        p = TProjectile()
        with rtw.DirectXWindow(p.canv) as w:
            p.launch()

    @pytest.mark.skip("VulkanWindow has not been implemented")
    def test_projectile_anim_live_vulkan(self):
        p = TProjectile()
        with rtw.VulkanWindow(p.canv) as w:
            p.launch()


class TestCh3:
    def test_matrix(self):
        m = rtn.Matrix([
            [1, 2, 3, 4],
            [5.5, 6.5, 7.5, 8.5],
            [9, 10, 11, 12],
            [13.5, 14.5, 15.5, 16.5]
        ])

        assert m[0][0] == 1
        assert m[0][1] == 2
        assert m[0][2] == 3
        assert m[0][3] == 4
        assert m[1][0] == 5.5
        assert m[1][1] == 6.5
        assert m[1][2] == 7.5
        assert m[1][3] == 8.5
        assert m[2][0] == 9
        assert m[2][1] == 10
        assert m[2][2] == 11
        assert m[2][3] == 12
        assert m[3][0] == 13.5
        assert m[3][1] == 14.5
        assert m[3][2] == 15.5
        assert m[3][3] == 16.5

        m = rtn.Matrix([
            [-3, 5],
            [1, -2]
        ])

        assert m[0][0] == -3
        assert m[0][1] == 5
        assert m[1][0] == 1
        assert m[1][1] == -2

        m = rtn.Matrix([
            [-3, 5, 0],
            [1, -2, -7],
            [0, 1, 1]
        ])

        assert m[0][0] == -3
        assert m[0][1] == 5
        assert m[0][2] == 0
        assert m[1][0] == 1
        assert m[1][1] == -2
        assert m[1][2] == -7
        assert m[2][0] == 0
        assert m[2][1] == 1
        assert m[2][2] == 1

    def test_matrix_cmp(self):
        a = rtn.Matrix([
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 8, 7, 6],
            [5, 4, 3, 2]
        ])

        b = rtn.Matrix([
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 8, 7, 6],
            [5, 4, 3, 2]
        ])

        assert a == b

        b = rtn.Matrix([
            [2, 3, 4, 5],
            [6, 7, 8, 9],
            [8, 7, 6, 5],
            [4, 3, 2, 1]
        ])

        assert a != b

    def test_matrix_mult(self):
        a = rtn.Matrix([
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 8, 7, 6],
            [5, 4, 3, 2]
        ])

        b = rtn.Matrix([
            [-2, 1, 2, 3],
            [3, 2, 1, -1],
            [4, 3, 6, 5],
            [1, 2, 7, 8]
        ])

        c = rtn.Matrix([
            [20, 22, 50, 48],
            [44, 54, 114, 108],
            [40, 58, 110, 102],
            [16, 26, 46, 42]
        ])

        assert a @ b == c

    def test_matrix_tuple_mult(self):
        a = rtn.Matrix([
            [1, 2, 3, 4],
            [2, 4, 4, 2],
            [8, 6, 4, 1],
            [0, 0, 0, 1]
        ])

        b = rtn.NpFourTuple(1, 2, 3, 1)

        assert a @ b == rtn.NpFourTuple(18, 24, 33, 1)

    def test_matrix_eye(self):
        a = rtn.Matrix([
            [0, 1, 2, 4],
            [1, 2, 4, 8],
            [2, 4, 8, 16],
            [4, 8, 16, 32]
        ])

        assert a @ rtn.Matrix.identity(4) == a
        assert rtn.Matrix.identity(4) @ a == a

        a = rtn.NpFourTuple(1, 2, 3, 4)
        assert a @ rtn.Matrix.identity(4) == a

    def test_matrix_transpose(self):
        a = rtn.Matrix([
            [0, 9, 3, 0],
            [9, 8, 0, 8],
            [1, 8, 5, 3],
            [0, 0, 5, 8]
        ])

        b = rtn.Matrix([
            [0, 9, 1, 0],
            [9, 8, 8, 0],
            [3, 0, 5, 5],
            [0, 8, 3, 8]
        ])

        assert a.transpose == b
        assert b.transpose == a

    def test_matrix_eye_transpose(self):
        assert rtn.Matrix.identity(5).transpose == rtn.Matrix.identity(5)
        assert rtn.Matrix.identity(5) == rtn.Matrix.identity(5).transpose

    def test_matrix_simple_det(self):
        a = rtn.Matrix([
            [1, 5],
            [-3, 2]
        ])

        assert a.det == 17

    def test_submatrix(self):
        a = rtn.Matrix([
            [1, 5, 0],
            [-3, 2, 7],
            [0, 6, -3]
        ])

        a2 = rtn.Matrix([
            [-3, 2],
            [0, 6]
        ])

        assert a.sub(0, 2) == a2

        b = rtn.Matrix([
            [-6, 1, 1, 6],
            [-8, 5, 8, 6],
            [-1, 0, 8, 2],
            [-7, 1, -1, 1]
        ])

        b2 = rtn.Matrix([
            [-6, 1, 6],
            [-8, 8, 6],
            [-7, -1, 1]
        ])

        assert b.sub(2, 1) == b2

    def test_minor(self):
        a = rtn.Matrix([
            [3, 5, 0],
            [2, -1, -7],
            [6, -1, 5]
        ])
        b = a.sub(1, 0)
        assert round(b.det) == 25
        assert round(a.minor(1, 0)) == 25

    def test_cofactor(self):
        a = rtn.Matrix([
            [3, 5, 0],
            [2, -1, -7],
            [6, -1, 5]
        ])

        assert round(a.minor(0, 0)) == -12
        assert round(a.cofactor(0, 0)) == -12
        assert round(a.minor(1, 0)) == 25
        assert round(a.cofactor(1, 0)) == -25

    def test_matrix_det(self):
        a = rtn.Matrix([
            [1, 2, 6],
            [-5, 8, -4],
            [2, 6, 4]
        ])

        assert round(a.cofactor(0, 0)) == 56
        assert round(a.cofactor(0, 1)) == 12
        assert round(a.cofactor(0, 2)) == -46
        assert round(a.det) == -196

        b = rtn.Matrix([
            [-2, -8, 3, 5],
            [-3, 1, 7, 3],
            [1, 2, -9, 6],
            [-6, 7, 7, -9]
        ])

        assert round(b.cofactor(0, 0)) == 690
        assert round(b.cofactor(0, 1)) == 447
        assert round(b.cofactor(0, 2)) == 210
        assert round(b.cofactor(0, 3)) == 51
        assert round(b.det) == -4071

    def test_matrix_invertible(self):
        a = rtn.Matrix([
            [6, 4, 4, 4],
            [5, 5, 7, 6],
            [4, -9, 3, -7],
            [9, 1, 7, -6]
        ])

        assert round(a.det) == -2120
        assert a.invertible

        b = rtn.Matrix([
            [4, 2, -2, -3],
            [9, 6, 2, 6],
            [0, -5, 1, -5],
            [0, 0, 0, 0]
        ])

        assert b.det == 0
        assert not b.invertible

    def test_matrix_inverse(self):
        a = rtn.Matrix([
            [-5, 2, 6, -8],
            [1, -5, 1, 8],
            [7, 7, -6, -7],
            [1, -3, 7, 4]
        ])

        b = a.inv

        assert round(a.det) == 532
        assert round(a.cofactor(2, 3)) == -160
        assert b[3][2] == -160/532
        assert round(a.cofactor(3, 2)) == 105
        assert b[2][3] == 105/532
        assert round(b, 5) == rtn.Matrix([
            [0.21805, 0.45113, 0.24060, -0.04511],
            [-0.80827, -1.45677, -0.44361, 0.52068],
            [-0.07895, -0.22368, -0.05263, 0.19737],
            [-0.52256, -0.81391, -0.30075, 0.30639]
        ])

        c = rtn.Matrix([
            [8, -5, 9, 2],
            [7, 5, 6, 1],
            [-6, 0, 9, 6],
            [-3, 0, -9, -4]
        ])

        assert round(c.inv, 5) == rtn.Matrix([
            [-0.15385, -0.15385, -0.28205, -0.53846],
            [-0.07692, 0.12308, 0.02564, 0.03077],
            [0.35897, 0.35897, 0.43590, 0.92308],
            [-0.69231, -0.69231, -0.76923, -1.92308]
        ])

        d = rtn.Matrix([
            [9, 3, 0, 9],
            [-5, -2, -6, -3],
            [-4, 9, 6, 4],
            [-7, 6, 6, 2]
        ])

        assert round(d.inv, 5) == rtn.Matrix([
            [-0.04074, -0.07778, 0.14444, -0.22222],
            [-0.07778, 0.03333, 0.36667, -0.33333],
            [-0.02901, -0.14630, -0.10926, 0.12963],
            [0.17778, 0.06667, -0.26667, 0.33333]
        ])

    def test_matrix_product_and_inverse(self):
        a = rtn.Matrix([
            [3, -9, 7, 3],
            [3, -8, 2, -9],
            [-4, 4, 4, 1],
            [-6, 5, -1, 1]
        ])

        b = rtn.Matrix([
            [8, 2, 2, 2],
            [3, -1, 7, 0],
            [7, 0, 5, 4],
            [6, -2, 0, 5]
        ])

        c = a @ b

        assert round(c @ b.inv) == a


class TestCh4:
    def test_translation(self):
        transform = rtn.translation(5, -3, 2)
        p = rtn.Point(-3, 4, 5)
        assert transform @ p == rtn.Point(2, 1, 7)
        assert p.translate(rtn.Vector(5, -3, 2)) == rtn.Point(2, 1, 7)

        inv = transform.inv
        assert inv @ p == rtn.Point(-8, 7, 3)
        assert p.translate(rtn.Vector(-5, 3, -2)) == rtn.Point(-8, 7, 3)

        v = rtn.Vector(-3, 4, 5)
        assert transform @ v == v

    def test_scaling(self):
        transform = rtn.scaling(2, 3, 4)
        p = rtn.Point(-4, 6, 8)
        assert transform @ p == rtn.Point(-8, 18, 32)
        assert p.scale(rtn.Vector(2, 3, 4)) == rtn.Point(-8, 18, 32)

        v = rtn.Vector(-4, 6, 8)
        assert transform @ v == rtn.Vector(-8, 18, 32)
        assert v.scale(rtn.Vector(2, 3, 4)) == rtn.Vector(-8, 18, 32)

        inv = transform.inv
        assert inv @ p == rtn.Point(-2, 2, 2)
        assert inv @ v == rtn.Vector(-2, 2, 2)

        # reflection is a special case of scaling
        p = rtn.Point(2, 3, 4)
        assert rtn.x_reflection() @ p == rtn.Point(-2, 3, 4)
        assert rtn.y_reflection() @ p == rtn.Point(2, -3, 4)
        assert rtn.z_reflection() @ p == rtn.Point(2, 3, -4)

    def test_rotation(self):
        sqrt2over2 = round(math.sqrt(2) / 2, 6)
        piover4 = math.pi / 4
        piover2 = math.pi / 2

        # x
        p = rtn.Point(0, 1, 0)
        v = rtn.Vector(0, 1, 0)
        half_quarter = rtn.x_rotation(piover4)
        full_quarter = rtn.x_rotation(piover2)
        assert round(half_quarter @ p, 6) == rtn.Point(0,
                                                       sqrt2over2, sqrt2over2)
        assert round(p.rotate_x(piover4), 6) == rtn.Point(
            0, sqrt2over2, sqrt2over2)
        assert round(full_quarter @ p, 6) == rtn.Point(0, 0, 1)
        assert round(p.rotate_x(piover2), 6) == rtn.Point(0, 0, 1)
        assert round(half_quarter @ v, 6) == rtn.Vector(0,
                                                        sqrt2over2, sqrt2over2)
        assert round(v.rotate_x(piover4), 6) == rtn.Vector(
            0, sqrt2over2, sqrt2over2)
        assert round(full_quarter @ v, 6) == rtn.Vector(0, 0, 1)
        assert round(v.rotate_x(piover2), 6) == rtn.Vector(0, 0, 1)

        # y
        p = rtn.Point(0, 0, 1)
        v = rtn.Vector(0, 0, 1)
        half_quarter = rtn.y_rotation(piover4)
        full_quarter = rtn.y_rotation(piover2)
        assert round(half_quarter @ p,
                     6) == rtn.Point(sqrt2over2, 0, sqrt2over2)
        assert round(p.rotate_y(piover4), 6) == rtn.Point(
            sqrt2over2, 0, sqrt2over2)
        assert round(full_quarter @ p, 6) == rtn.Point(1, 0, 0)
        assert round(p.rotate_y(piover2), 6) == rtn.Point(1, 0, 0)
        assert round(half_quarter @ v, 6) == rtn.Vector(sqrt2over2,
                                                        0, sqrt2over2)
        assert round(v.rotate_y(piover4), 6) == rtn.Vector(
            sqrt2over2, 0, sqrt2over2)
        assert round(full_quarter @ v, 6) == rtn.Vector(1, 0, 0)
        assert round(v.rotate_y(piover2), 6) == rtn.Vector(1, 0, 0)

        # z
        p = rtn.Point(1, 0, 0)
        v = rtn.Vector(1, 0, 0)
        half_quarter = rtn.z_rotation(piover4)
        full_quarter = rtn.z_rotation(piover2)
        assert round(half_quarter @ p,
                     6) == rtn.Point(sqrt2over2, sqrt2over2, 0)
        assert round(p.rotate_z(piover4), 6) == rtn.Point(
            sqrt2over2, sqrt2over2, 0)
        assert round(full_quarter @ p, 6) == rtn.Point(0, 1, 0)
        assert round(p.rotate_z(piover2), 6) == rtn.Point(0, 1, 0)
        assert round(half_quarter @ v,
                     6) == rtn.Vector(sqrt2over2, sqrt2over2, 0)
        assert round(v.rotate_z(piover4), 6) == rtn.Vector(
            sqrt2over2, sqrt2over2, 0)
        assert round(full_quarter @ v, 6) == rtn.Vector(0, 1, 0)
        assert round(v.rotate_z(piover2), 6) == rtn.Vector(0, 1, 0)

    def test_shearing(self):
        p = rtn.Point(2, 3, 4)
        assert rtn.shearing(1, 0, 0, 0, 0, 0) @ p == rtn.Point(5, 3, 4)
        assert rtn.shearing(0, 1, 0, 0, 0, 0) @ p == rtn.Point(6, 3, 4)
        assert rtn.shearing(0, 0, 1, 0, 0, 0) @ p == rtn.Point(2, 5, 4)
        assert rtn.shearing(0, 0, 0, 1, 0, 0) @ p == rtn.Point(2, 7, 4)
        assert rtn.shearing(0, 0, 0, 0, 1, 0) @ p == rtn.Point(2, 3, 6)
        assert rtn.shearing(0, 0, 0, 0, 0, 1) @ p == rtn.Point(2, 3, 7)

    def test_chaining(self):
        p = rtn.Point(1, 0, 1)
        A = rtn.x_rotation(math.pi / 2)
        B = rtn.scaling(5, 5, 5)
        C = rtn.translation(10, 5, 7)

        p2 = A @ p
        assert round(p2, 10) == rtn.Point(1, -1, 0)

        p3 = B @ p2
        assert round(p3, 10) == rtn.Point(5, -5, 0)

        p4 = C @ p3
        assert round(p4, 10) == rtn.Point(15, 0, 7)

        assert C @ B @ A @ p == rtn.Point(15, 0, 7)


class TestCh5:
    pass


class TestCh6:
    pass


class TestCh7:
    @pytest.mark.skip("TClock has not been implemented")
    def test_clock_anim_live_opencv(self):
        c = TClock(bgr=True)
        with rtw.OpenCVWindow(c.canv) as w:
            c.day()

    @pytest.mark.skip("OpenGLWindow has not been implemented")
    def test_projectile_anim_live_opengl(self):
        c = TClock()
        with rtw.OpenCVWindow(c.canv) as w:
            c.day()

    @pytest.mark.skip("DirectXWindow has not been implemented")
    def test_projectile_anim_live_directx(self):
        c = TClock()
        with rtw.OpenCVWindow(c.canv) as w:
            c.day()

    @pytest.mark.skip("VulkanWindow has not been implemented")
    def test_projectile_anim_live_vulkan(self):
        c = TClock()
        with rtw.OpenCVWindow(c.canv) as w:
            c.day()


class TestCh8:
    pass


class TestCh9:
    pass


class TestCh10:
    pass


class TestCh11:
    pass


class TestCh12:
    pass


class TestCh13:
    pass


class TestCh14:
    pass


class TestCh15:
    pass


class TestCh16:
    pass

import os
import pytest
from conftest import *

from typing import Tuple, List, Callable, Dict
from random import choice, randrange

from util import *
from mazes.cell import *
from mazes.distances import *
from mazes.mask import *
from mazes.grid import *
from mazes.algo import *


def test_maze(algo: Callable[..., Grid] = BinaryTree, rows: int = 4, cols: int = 4) -> Grid:
    return algo(Grid(rows, cols))


def test_maze_img(algo: Callable[..., Grid] = BinaryTree, rows: int = 4, cols: int = 4) -> Image:
    return test_maze(algo, rows, cols).as_image()


def test_mask(rows: int = 5, cols: int = 5) -> MaskedGrid:
    mask = Mask(rows, cols)
    mask.bits[0][0] = False
    mask.bits[int(rows / 2)][int(cols / 2)] = False
    mask.bits[-1][-1] = False
    g = MaskedGrid(mask)
    RecursiveBacktracker(g)
    return g


@TXT_MASKS
def test_mask_from_txt(datafiles) -> List[MaskedGrid]:
    res = []
    for fname in datafiles.listdir():
        mask = Mask.from_txt(str(fname))
        g = MaskedGrid(mask)
        RecursiveBacktracker(g)
        res.append(g)
    return res


@IMG_MASKS
def test_mask_from_img(datafiles) -> List[MaskedGrid]:
    res = []
    for fname in datafiles.listdir():
        mask = Mask.from_img(str(fname))
        g = MaskedGrid(mask)
        RecursiveBacktracker(g)
        res.append(g)
    return res


def test_polar_grid_img(rows: int = 8) -> Image:
    g = PolarGrid(rows)
    RecursiveBacktracker(g)
    return g.as_image()


def test_hex_grid_img(rows: int = 10, cols: int = 10) -> Image:
    g = HexGrid(rows, cols)
    RecursiveBacktracker(g)
    return g.as_image()


def test_triangle_grid_img(rows: int = 10, cols: int = 17) -> Image:
    g = TriangleGrid(rows, cols)
    RecursiveBacktracker(g)
    return g.as_image()


# TODO: parameterize all functions after this line


# NOTE Dijkstra's algorithm is fairly complicated in this implementation.
# test_dijkstra_setup, test_dijkstra, test_shortest_path, and test_longest_path
# all use a bare-bones workflow to test Distances, DistanceGrid, and Cell.dijkstra.
# test_dijkstra_colorized and test_dijkstra_colorized_with_distances use
# a simpler, higher-level workflow based on Grid.dijkstra to test visualization.

def test_dijkstra_setup() -> Tuple[Grid, Distances]:
    g = DistanceGrid(5, 5)
    BinaryTree(g)
    start = g(0, 0)  # NW corner
    distances = start.dijkstra()
    g.distances = distances
    return g, distances


def test_dijkstra() -> str:
    g = test_dijkstra_setup()[0]
    return str(g)


def test_shortest_path() -> str:
    g, distances = test_dijkstra_setup()
    # shortest path from NW corner to SW corner
    g.distances = distances.path_to(g(g.rows - 1, 0))
    return str(g)


def test_longest_path() -> str:
    g, distances = test_dijkstra_setup()
    new_start, distance = distances.max()
    new_distances = new_start.dijkstra()
    goal, distance = new_distances.max()
    g.distances = new_distances.path_to(goal)
    return str(g)


def test_dijkstra_colorized() -> Image:
    g = ColoredGrid(25, 25)
    BinaryTree(g).dijkstra()
    return g.as_image(20)


def test_dijkstra_colorized_with_distances() -> Image:
    g = ColoredDistanceGrid(5, 5)
    BinaryTree(g).dijkstra()
    return g.as_image(40)


def test_weighted_grid_img() -> Image:
    g = WeightedGrid(10, 10)
    RecursiveBacktracker(g)
    g.braid(0.5)
    start, fin = (0, 0), (g.rows - 1, g.columns - 1)
    g.path(start, fin)
    # choice stumbles on sequences that don't support integer indexing,
    # including built-in dicts with string keys, and built-in dicts with integer
    # keys that don't match the corresponding integer list indices. not sure why.
    lava = choice(list(g.distances))
    lava.weight = 50
    g.path(start, fin)
    return g.as_image()


def test_grid_inset(inset: int = 0.1) -> Image:
    g = test_maze(RecursiveBacktracker, 20, 20)
    return g.as_image(inset=inset)


def test_weave_grid_img(rows: int = 20, cols: int = 20, inset: int = DEFAULT_WEAVE_GRID_INSET) -> Image:
    g = WeaveGrid(rows, cols)
    RecursiveBacktracker(g)
    return g.as_image(inset=inset)


def test_kruskals_img(rows: int = 20, cols: int = 20, max_crossings: Optional[int] = None) -> Image:
    if max_crossings is None:
        max_crossings = rows * cols

    grid = PreconfiguredGrid(rows, cols)
    state = Kruskals.State(grid)

    for i in range(max_crossings):
        row = 1 + randrange(grid.rows - 2)
        col = 1 + randrange(grid.columns - 2)
        state.add_crossing(grid(row, col))

    Kruskals(grid, state)

    return grid.as_image(inset=0.2)


def test_grid3d_img(levels: int = 5, rows: int = 5, cols: int = 5) -> Image:
    g = Grid3D(levels, rows, cols)
    RecursiveBacktracker(g)
    return g.as_image()


def test_cylinder_grid_img() -> Image:
    g = CylinderGrid(7, 16)
    RecursiveBacktracker(g)
    return g.as_image()


def test_moebius_grid_img() -> Image:
    g = MoebiusGrid(5, 50)
    RecursiveBacktracker(g)
    return g.as_image()


def test_cube_grid_img() -> Image:
    g = CubeGrid(10)
    RecursiveBacktracker(g)
    return g.as_image()


def test_sphere_grid_img() -> Image:
    g = SphereGrid(20)
    RecursiveBacktracker(g)
    return g.as_image()


# TODO add backgrounds and distances for grids with insets, and also tests
# TODO see about doing the same for weave grids
# TODO add function for colorizing arbitrary grids, helps vizualize textures
# for stuff that doesn't fit neatly into ColoredDistanceGrid
# TODO reconsider class and function structure, i'm seeing a lot of boilerplate
# within the various test functions, especially the ones that do images

def test_deadend_counts() -> None:
    algorithms = [BinaryTree, Sidewinder, AldousBroder,
                  Wilsons, HuntAndKill, RecursiveBacktracker,
                  Kruskals, SimplifiedPrims, TruePrims, Ellers,
                  RecursiveDivision]

    tries = 20
    size = 5

    averages = {}

    for a in algorithms:
        print("running " + a.__name__ + "...")

        deadend_counts = []
        for i in range(tries):
            grid = Grid(size, size)
            a(grid)
            deadend_counts.append(len(grid.deadends()))

        averages[a] = sum(deadend_counts) / len(deadend_counts)

    print(f"Average deadends per {size}x{size} maze ({size*size} cells):")

    sorted_algorithms = {key: value for key, value in sorted(
        averages.items(), key=lambda item: item[1], reverse=True)}

    for alg, avg in sorted_algorithms.items():
        percentage = (avg * 100.0) / (size*size)
        # i absolutely adore the new format strings
        print(
            f"{alg.__name__:14s} : {round(avg):#3d}/{size*size:#d} ({round(percentage):#d}%)")
